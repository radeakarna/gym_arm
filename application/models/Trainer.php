<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trainer extends CI_Model {

  public $table = 'trainer';
  
  public function __construct()
  {
    parent::__construct();
  }

  public function get()
  {
    $this->db->order_by('id','DESC');
    return $this->db->get($this->table)->result();
  }
  
  public function find_by_id($id)
  {
    return $this->db->where('id', $id)->limit(1)->get($this->table)->row();
  }

  public function find_where($where)
  {
    return $this->db->where($where)->get($this->table)->result();
  }

}