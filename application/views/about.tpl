{extends file="layouts/index.tpl"}
{$tittle}
{block name=content}
<style type="text/css">
.breadcrumb_block{
    background-color:#28a745;
}
</style>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
    <div class="container">
        <div class="row justify-content-center" style="padding-top:100px;">
            <div class="col-md-4">
                <div class="breadcrumb_inner">
                    <h3>About us</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb_block">
        <ul>
            <li><a href="index.html">home</a></li>
            <li>about us</li>
        </ul>
    </div>
</div>
<!--Blog With Sidebar-->
<div class="blog_sidebar_wrapper clv_section blog_single_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="blog_left_section">
                    <div class="blog_section">
                        <div class="agri_blog_image">
                            <!-- <img src="https://via.placeholder.com/870x410" alt="image"> -->
                            <!-- <span class="agri_blog_date">About us</span> -->
                        </div>
                        <div class="agri_blog_content">
                            {get_metsos('about')}
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
{/block}