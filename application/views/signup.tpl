{extends file="layouts/index.tpl"}
{$tittle}
{block name=content}
<style type="text/css">
.breadcrumb_block{
    background-color:#28a745;
}
</style>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
    <div class="container">
        <div class="row justify-content-center" style="padding-top:100px;">
            <div class="col-md-4">
                <div class="breadcrumb_inner">
                    <h3>Member</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb_block">
        <ul>
            <li><a href="index.html">home</a></li>
            <li>member</li>
        </ul>
    </div>
</div>
<!--Blog With Sidebar-->
<div class="blog_sidebar_wrapper clv_section blog_single_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="blog_left_section">
                    <div class="blog_section">
                        <div class="agri_blog_image">
                            <!-- <img src="https://via.placeholder.com/870x410" alt="image"> -->
                            <!-- <span class="agri_blog_date">About us</span> -->
                        </div>
                        <div class="agri_blog_content">
                            <div class="checkout_heading">
                                <h3>basic information</h3>
                            </div>
                            <div class="profile_form">
                                <div class="row">
                                    <form id="myForm" method="POST">
                                        <input type="hidden" name="{$token}" id="signin_token" value="{$value}" style="display: none">
                                        <div class="form_block">
                                            <input type="text" class="form_field" placeholder="name" name="name" required/>
                                            <span id="error_name"></span>
                                        </div>
                                        <div class="form_block">
                                            <input type="email" class="form_field" placeholder="Email" name="email" required/>
                                            <span id="error_email"></span>
                                        </div>
                                        <div class="form_block">
                                            <input type="password" class="form_field" placeholder="Password" name="password" required/>
                                            <span id="error_password"></span>
                                        </div>
                                        <div class="form_block">
                                            <input type="password" class="form_field" placeholder="Confirm password" name="confirm_password" required/>
                                            <span id="error_confirm_password"></span>
                                        </div>
                                        <div class="form_block">
                                            <select class="form_field" name="jenis" required/>
                                                <option>--- Jenis member ---</option>
                                                {foreach from=$member item=$row}
                                                <option value='{$row->id}'>{$row->name}</option>
                                                {/foreach}
                                            </select>
                                            <span id="error_type"></span>
                                        </div>
                                        <div class="form_block" style="top:20px !important;clear:both">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form_block">
                                                <p id='getcapcha'>{$getCaptcha}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form_block">
                                                <input type="text" name="captcha" class="form_field"  placeholder="captcha" required/>
                                                <span id="error_captcha"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form_block">
                                            <button type="button" onclick="insert()" class="clv_btn submitForm" value="signin" data-type="contact">sign up</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
{/block}