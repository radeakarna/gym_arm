{extends file="layouts/index.tpl"}
{$tittle}
{block name=content}
<style type="text/css">
.breadcrumb_block{
    background-color:#28a745;
}
</style>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
    <div class="container">
        <div class="row justify-content-center" style="padding-top:100px;">
            <div class="col-md-4">
                <div class="breadcrumb_inner">
                    <h3>trainer</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb_block">
        <ul>
            <li><a href="index.html">home</a></li>
            <li>trainer</li>
        </ul>
    </div>
</div>
<!--User Profile-->

    {foreach from=$member item=$row}
    <div class="user_profile_wrapper clv_section">
        <div class="container">
            <div class="user_profile_section">
                <div class="profile_image_block">
                    <div class="user_profile_img">
                        <img src="{base_url()}assets/user/images/trainer/{$row->foto}" alt="image">
                    </div>
                </div>
                <div class="checkout_heading">
                    <h3>information</h3>
                </div>
                <div class="profile_form">
                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <div class="form_block">
                                <h6>Nama</h6>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <div class="form_block">
                                <span>{$row->nama}</span>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <div class="form_block">
                                <h6>Jenis kelamin</h6>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <div class="form_block">
                                <span>{$row->jk}</span>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <div class="form_block">
                                <h6>Alamat</h6>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <div class="form_block">
                                <span>{$row->alamat}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {/foreach}

{/block}
{block name=footer}
        
{/block}