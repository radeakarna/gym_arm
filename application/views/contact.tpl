{extends file="layouts/index.tpl"}
{$tittle}
{block name=content}
<style type="text/css">
.breadcrumb_block{
    background-color:#28a745;
}
</style>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
    <div class="container">
        <div class="row justify-content-center" style="padding-top:100px;">
            <div class="col-md-4">
                <div class="breadcrumb_inner">
                    <h3>contact us</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb_block">
        <ul>
            <li><a href="index.html">home</a></li>
            <li>contact us</li>
        </ul>
    </div>
</div>
<!--Contact Block-->
<div class="contact_blocks_wrapper clv_section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="contact_block">
                    <span></span>
                    <div class="contact_icon"><img src="{base_url()}assets/user/images/contact_icon1.png" alt="image" /></div>
                    <h4>contact us</h4>
                    <p>{get_metsos('contact')}</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="contact_block">
                    <span></span>
                    <div class="contact_icon"><img src="{base_url()}assets/user/images/contact_icon2.png" alt="image" /></div>
                    <h4>email</h4>
                    <p>{get_metsos('gmail')}</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="contact_block">
                    <span></span>
                    <div class="contact_icon"><img src="{base_url()}assets/user/images/contact_icon3.png" alt="image" /></div>
                    <h4>address</h4>
                    <p>{get_metsos('addres_gym')}</p>
                </div>
            </div>
        </div>
    </div>
</div>

{/block}
{block name=footer}
        
{/block}