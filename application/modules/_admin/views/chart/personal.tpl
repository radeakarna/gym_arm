{extends file="index.tpl"}
{block name=header}
<!-- Morris Charts CSS -->
<link href="{base_url()}plugins/moris_chart/morris.css" rel="stylesheet">
{/block}
{block name=content}
<section class="content">
    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan harian</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan bulanan</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart-bulan" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan Pertahun</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart-tahun" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

</section><!-- /.content -->
{/block}
{block name=footer}
<!-- Morris Charts JavaScript -->
<script src="{base_url()}plugins/moris_chart/raphael.min.js"></script>
<script src="{base_url()}plugins/moris_chart/morris.min.js"></script>
<script type="text/javascript">
$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"{base_url()}_admin/chart_controller/get_personal_data_day",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});

$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart-bulan',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"{base_url()}_admin/chart_controller/get_personal_data_month",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});

$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart-tahun',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"{base_url()}_admin/chart_controller/get_personal_data_year",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});
</script>

{/block}