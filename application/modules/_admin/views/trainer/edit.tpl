{extends file="index.tpl"}
{block name=header}
<link rel="stylesheet" href="{base_url()}plugins/datetimepicker/css/bootstrap-datepicker.css">
{/block}
{block name=content}
{if isset($message)}
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p>{$message}</p>
    </div>
  </section>
{/if}
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Edit Trainer</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="{base_url()}_admin/trainer/update/{$get_trainer->id}" enctype='multipart/form-data'>
        <div class="box-body">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="{$get_trainer->nama}" required/>
              {form_error('name')}
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Jenis kelamin</label>
              <select name="jk" class="form-control">
                <option value="laki-laki" {if $get_trainer->jk == 'laki-laki'} selected {/if}>laki-laki</option>
                <option value="perempuan" {if $get_trainer->jk == 'perempuan'} selected {/if}>perempuan</option>
              </select>
              {form_error('jk')}
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Alamat</label>
              <textarea name="alamat" class="form-control" rows="4">{$get_trainer->alamat}</textarea>
              {form_error('alamat')}
            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Foto</label>
              <input type="file" name="foto" required/>
              {form_error('foto')}
            </div>
          
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
{/block}