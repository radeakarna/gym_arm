{extends file="index.tpl"}
{block name=content}
  {if isset($message)}
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-info">
        <h4>INFO!</h4>
        <p>{$message}</p>
      </div>
    </section>
  {/if}
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-md-4">
            <form method="POST" action="{base_url()}_admin/settings_controller/update_cost_personal">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Biaya member personal</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                <input type="text" name="member" value="{get_metsos('personal')}" class="form-control" required>
              </div><!-- ./box-body -->
              <div class="box-footer">
                <input type="submit" class="btn btn-info pull-right" name="submit" value="Ubah">     
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->          
          </form>
          <form method="POST" action="{base_url()}_admin/settings_controller/update_cost_personal_trainer">
          <div class="col-md-4">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Biaya member personal + trainer</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                <input type="text" name="member" value="{get_metsos('personal trainer')}" class="form-control" required>
              </div><!-- ./box-body -->
              <div class="box-footer">
                <input type="submit" class="btn btn-info pull-right" name="submit" value="Ubah">     
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->          
          </form>
          <form method="POST" action="{base_url()}_admin/settings_controller/update_cost_member_day">
          <div class="col-md-4">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Biaya member harian</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                <input type="text" name="member" value="{get_metsos('member_day')}" class="form-control" required>
              </div><!-- ./box-body -->
              <div class="box-footer">
                <input type="submit" class="btn btn-info pull-right" name="submit" value="Ubah">     
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->          
        </diV>
    </section><!-- /.content -->
    </form>
{/block}

