{extends file="index.tpl"}
{block name=content}
  <style>
      .kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
          margin: 0;
          padding: 0;
          border: none;
          box-shadow: none;
          text-align: center;
      }
      .kv-avatar .file-input {
          display: table-cell;
          max-width: 250px;
      }
      .kv-reqd {
          color: red;
          font-family: monospace;
          font-weight: normal;
      }
  </style>
  {if isset($message)}
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-success">
        <h4>INFO!</h4>
        <p>{$message}</p>
      </div>
    </section>
  {/if}
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-md-6">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Sosial Meida</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-10">
                    <p class="margin">Facebok</code></p>
                    <form method="POST" action="{base_url()}_admin/setting/update_facebook/">
                    <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="url" name="facebook" value="{get_metsos('facebook')}" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                    <p class="margin">Instragram</p>
                    <form method="POST" action="{base_url()}_admin/setting/update_instagram">
                    <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="url" name="instragram" value="{get_metsos('instagram')}" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                    <p class="margin">Twitter</p>
                    <form method="POST" action="{base_url()}_admin/setting/update_twitter">
                    <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="url" name="twitter" value="{get_metsos('twitter')}" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->

                    </form>

                  </div><!-- /.col -->
                </div><!-- /.row -->
              </div><!-- ./box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->

          <div class="col-md-6">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Contact person</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-10">
                    <p class="margin">Nomor hp</code></p>
                    <form method="POST" action="{base_url()}_admin/setting/update_contact">
                    <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="text" name="contact" value="{get_metsos('contact')}" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                    <p class="margin">Gmail</p>
                    <form method="POST" action="{base_url()}_admin/setting/update_gmail">
                    <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="email" name="gmail" value="{get_metsos('gmail')}" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                    <p class="margin">Alamat</p>
                    <form method="POST" action="{base_url()}_admin/setting/update_addres">
                    <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="text" name="addres" value="{get_metsos('addres_gym')}" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                  </div><!-- /.col -->
                </div><!-- /.row -->
              </div><!-- ./box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->
        </diV>
    </section><!-- /.content -->
{/block}
{block name=footer}
    <!-- validator -->
    <script src="{base_url()}plugins/validator.js" type="text/javascript"></script>
    <script type="text/javascript">
      var menu_oTables="";
      $(document).ready(function(){
        menu_oTables = $('#extmdm');
      });
      function reload_table(){
          menu_oTables.ajax.reload(null,false); //reload datatable ajax 
      }
      function hpsdt(id_emu){
       // ajax delete data to database
        swal({
            title: 'Are you sure?',
            text: "You won't be able to delete this!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              $.ajax({
                type: "POST",
                data: {
                  id:id_emu
                },
                dataType:"HTML",
                url: "hapusdata",
                  success: function(data) {
                      reload_table();
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    swal("Error deleting!", "Please try again", "error");
                  }
              });
            }
              
          });
      }
    </script>
{/block}
