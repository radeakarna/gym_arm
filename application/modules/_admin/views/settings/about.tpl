{extends file="index.tpl"}
{block name=content}
  {if isset($message)}
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-success">
        <h4>INFO!</h4>
        <p>{$message}</p>
      </div>
    </section>
  {/if}
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">About US</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <form method="POST" action="{base_url()}_admin/setting/update_about">
                    <input type="hidden" name="{$token}" value="{$value}" style="display: none">
                    <div class="form-group">
                      <textarea class="form-control" name="about" id="about">{get_metsos('about')}</textarea>
                    </div>
                    <div class="form-group">
                      <input type="submit" class="btn btn-info" name="submit" value="submit">
                    </div>
                </form>
                <div class="row">


                
              </div><!-- ./box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->

          
        </diV>
    </section><!-- /.content -->
{/block}
{block name=footer}
<!-- Tiny Mce / untuk membuat tampilan type text menjadi tampilan blogspot -->
<script src="{base_url()}plugins/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: "#about",theme: "modern",height: 400,
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak",
             "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
             "table contextmenu directionality emoticons paste textcolor"
       ],
       toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
       toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
       image_advtab: true ,
       external_filemanager_path:"{base_url()}plugins/filemanager/",
       filemanager_title:"Responsive Filemanager" ,
       external_plugins: { "filemanager" : "../filemanager/plugin.min.js"}
    });
</script>    
{/block}
