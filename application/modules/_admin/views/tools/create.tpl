{extends file="index.tpl"}
{block name=content}
{if isset($message)}
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p>{$message}</p>
    </div>
  </section>
{/if}
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="{base_url()}_admin/tool/store">
        <div class="box-body">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="" required/>
                  {form_error('name')}
                </div>
                <div class="col-md-6">
                  <label for="exampleInputEmail1">jumlah barang</label>
                  <input type="number" class="form-control" name="jumlah" placeholder="Jumlah barang yang ada" value="" required/>
                  {form_error('jumlah')}
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Deskripsi</label>
                  <textarea name="deskripsi" class="form-control" rows="4"></textarea>
                  {form_error('deskripsi')}
                </div>
              </div>
            </div>
          
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
{/block}