{extends file="index.tpl"}
{block name=header}
<!-- data tables-->
    <link href="{base_url()}plugins/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
{/block}
{block name=content}
{if isset($message)}
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p>{$message}</p>
    </div>
  </section>
{/if}

<section class="content">
    <!-- <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Pembayaran</h3>
        
      </div>
      <div class="box-body">
        <form method="POST" action="{base_url()}_admin/personal_presences/store">
          <input type="hidden" id='csrf_test_name_two' name="{$token}" value="{$value}" style="display: none">
          <div class="input-group">
            <input type="text" class="form-control" name="id_member" id="id_member" placeholder="Kode member" value="" required/>
            <span class="input-group-btn">
              <button type="submit" class="btn btn-success btn-flat">Presensi</button>
            </span>
          </div>
        </form>
      </div>
    </div> -->
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Presensi</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                <a href="{base_url()}_admin/personal_presences/create"><button type="button" class="btn btn-info btn-flat btn-sm"><i class="fa fa-plus"></i> Tambah data</button></a>
            </div><!-- /. tools -->
        </div>
        <div class="box-body">
          <input type="hidden" id='{$token}' name="{$token}" value="{$value}" style="display: none">
          <div class="table-responsive">
            <table id="data-payment" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Code member</th>
                        <th>Tanggal Daftar</th>
                        <th><center>action</center></th>
                    </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>  
          </div>
        </div>
        <div class="box-footer clearfix">
          
        </div>
    </div>

</section><!-- /.content -->
{/block}
{block name=footer}
 <!-- data tables -->
<script src="{base_url()}plugins/media/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="{base_url()}plugins/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="{base_url()}assets/admin/js/member.js"></script>
<script type="text/javascript">
var menu_oTables="";
$(document).ready(function(){
  menu_oTables = $('#data-payment').DataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": true,

       "ajax": {
          "url": '{base_url()}_admin/personal_presences/show_member',
          "type": "POST",
          "dataType" : "json",
          data: function ( d ) {
              d.csrf_test_name = $('#csrf_test_name').val();
          },
          dataSrc: function(json){
            if (json.csrf_test_name !== undefined) $('meta[name=csrf_test_name]').attr("content", json.data.token_value);
            return json.data;
          },
       },
       "columns": [
          { 
            "data": "no" 
          },
          { 
            "data": "name" 
          },
          { 
            "data": "id" 
          },
          { 
            "data": "created_at" 
          },
          { 
            "data": "action" 
          }
          ]
  });
});
</script>
{/block}