{extends file="index.tpl"}
{block name=content}
{if isset($message)}
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p>{$message}</p>
    </div>
  </section>
{/if}
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="{base_url()}_admin/payment_personal/create">
        <div class="box-body">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="input-group">
              <input type="text" class="form-control" name="id_member" id="id_member" placeholder="Kode member" value="" required/>
              <span class="input-group-btn">
                <button type="submit" class="btn btn-success btn-flat">Bayar</button>
              </span>
            </div>
        </div>
        <div class="box-footer clearfix">
          <!-- <button type="submit" class="btn btn-primary">Simpan data</button> -->
        </div>
        </form>
    </div>
</section><!-- /.content -->
{/block}
{block name=footer}
<script type="text/javascript">
//     $(document).ready(function(){
//         $("#id_member").change(function(){
//             var id_member = $(this).val();
//             $.ajax({
//             type: "POST",
//             data: {
//                 csrf_test_name:{$value}, id:id_member
//             },
//             dataType:"JSON",
//             url: "{base_url()}_admin/payment_individu/find_user",
//               success: function(data) {
//                   $('[name="name"]').val(data.name);
//               },
//               error: function (jqXHR, textStatus, errorThrown)
//               {
//                 swal("Error showing!", "Please try again", "error");
//               }
//             });
//         });
//     })
</script>
{/block}