{extends file="index.tpl"}
{block name=header}
<link rel="stylesheet" href="{base_url()}plugins/datetimepicker/css/bootstrap-datepicker.css">
{/block}
{block name=content}
{if isset($message)}
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p>{$message}</p>
    </div>
  </section>
{/if}
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="{base_url()}_admin/personal_trainer/update/{$get_member->id_user}">
        <div class="box-body">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="{$get_member->name}" required/>
                  {form_error('name')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">email</label>
                  <input type="email" class="form-control" name="email" placeholder="Email aktif" value="{$get_member->username}" readonly required/>
                  {form_error('email')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Jenis member</label>
                  <select class="form-control" name="jenis" required/>
                    {foreach from=$get_member_tipe item=$row}
                      {if {$row->id} == $get_member->id_member}
                        <option value="{$row->id}" selected>{$row->name}</option>
                      {else}
                        <option value="{$row->id}">{$row->name}</option>
                      {/if}
                    {/foreach}
                  </select>
                  {form_error('jenis')}
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="password" class="form-control" name="password" value="" >
                  {form_error('password')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Confirm Password</label>
                  <input type="password" class="form-control" name="confirm_password" value="" >
                  {form_error('confirm_password')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Aktive member</label>
                  <input type="text" class="form-control" id="datepicker" name="akun" value="{$get_member->lost_member}" >
                  {form_error('akun')}
                </div>
              </div>
            </div>
            
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="{$get_member->nisn}" required/>
                  {form_error('nik')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="{$get_member->place_of_birth}" required/>
                  {form_error('tmp_lahir')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="{$get_member->date_of_birth}" required/>
                  {form_error('tgl_lahir')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" value="{$get_member->telp}" required/>
                  {form_error('no_telepon')}
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tinggi badan</label>
                  <input type="text" class="form-control" name="tinggi" placeholder="Tinggi basan" value="{$get_member->hight}" required/>
                  {form_error('tinggi')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Berat badan</label>
                  <input type="text" class="form-control" name="berat" placeholder="Berat badan" value="{$get_member->wight}" required/>
                  {form_error('berat')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  {form_error('jk')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4">{$get_member->addres}</textarea>
                  {form_error('alamat')}
                </div>
              </div>
            </div>
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
{/block}
{block name=footer}
<!-- Datetimepicker -->
<script src="{base_url()}plugins/datetimepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    //function get_tanggal(){
        $(function () {
            $('#datepicker').datepicker({
              startView: 2,
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });   
    //}      
</script>
{/block}