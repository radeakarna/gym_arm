{extends file="index.tpl"}
{block name=header}
<!-- data tables-->
    <link href="{base_url()}plugins/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
{/block}
{block name=content}
{if isset($message)}
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p>{$message}</p>
    </div>
  </section>
{/if}
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
              <a href="{base_url()}_admin/payment_personal_trainer/"><button type="button" class="btn btn-default btn-flat btn-sm"><i class="fa fa-money"></i> Pembayaran member</button></a>
                <a href="{base_url()}_admin/personal_trainer/create"><button type="button" class="btn btn-info btn-flat btn-sm"><i class="fa fa-plus"></i> Tambah data</button></a>
            </div><!-- /. tools -->
        </div>
        <div class="box-body">
          <form method="POST">
            <input type="hidden" id='csrf_test_name' name="{$token}" value="{$value}" style="display: none">
          </form>
          <div class="table-responsive">
            <table id="data-member" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Kode member</th>
                        <th>Jenis member</th>
                        <th>Usernae</th>
                        <th>Password</th>
                        <th>Tangal daftar</th>
                        <th>Habis member</th>
                        <th><center>action</center></th>
                    </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>  
          </div>
        </div>
        <div class="box-footer clearfix">
          
        </div>
    </div>

</section><!-- /.content -->
{/block}
{block name=footer}
 <!-- data tables -->
<script src="{base_url()}plugins/media/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="{base_url()}plugins/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="{base_url()}assets/admin/js/member.js"></script>
<script type="text/javascript">
var menu_oTables="";
$(document).ready(function(){
  menu_oTables = $('#data-member').DataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": true,

       "ajax": {
          "url": '{base_url()}_admin/personal_trainer/show',
          "type": "POST",
          "dataType" : "json",
          "data": function ( d ) {
              d.csrf_test_name = $('#csrf_test_name').val();
          },
       },
       "columns": [
          { 
            "data": "no" 
          },
          { 
            "data": "name" 
          },
          { 
            "data": "id" 
          },
          { 
            "data": "type" 
          },
          { 
            "data": "username" 
          },
          { 
            "data": "password" 
          },
          { 
            "data": "created_at" 
          },
          { 
            "data": "lost_member" 
          },
          {
           "data": "action" 
         },
          ]
  });
});
</script>
{/block}