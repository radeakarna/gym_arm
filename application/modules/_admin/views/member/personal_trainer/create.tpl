{extends file="index.tpl"}
{block name=header}
<link rel="stylesheet" href="{base_url()}plugins/datetimepicker/css/bootstrap-datepicker.css">
{/block}
{block name=content}
{if isset($message)}
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p>{$message}</p>
    </div>
  </section>
{/if}
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="{base_url()}_admin/personal_trainer/create">
        <div class="box-body">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="" required/>
                  {form_error('name')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">email</label>
                  <input type="email" class="form-control" name="email" placeholder="Email aktif" value="" required/>
                  {form_error('email')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="text" class="form-control" name="password" value="{$get_random_password}" required/>
                  {form_error('password')}
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="" required/>
                  {form_error('nik')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="" required/>
                  {form_error('tmp_lahir')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="" required/>
                  {form_error('tgl_lahir')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" required/>
                  {form_error('no_telepon')}
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tinggi badan</label>
                  <input type="text" class="form-control" name="tinggi" placeholder="Tinggi basan" value="" required/>
                  {form_error('tinggi')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Berat badan</label>
                  <input type="text" class="form-control" name="berat" placeholder="Berat badan" value="" required/>
                  {form_error('berat')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  {form_error('jk')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4"></textarea>
                  {form_error('alamat')}
                </div>
              </div>
            </div>
          
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
{/block}
{block name=footer}
<!-- Datetimepicker -->
<script src="{base_url()}plugins/datetimepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    //function get_tanggal(){
        $(function () {
            $('#datepicker').datepicker({
              startView: 2,
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });   
    //}      
</script>
{/block}