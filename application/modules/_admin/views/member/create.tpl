{extends file="index.tpl"}
{block name=content}
{if isset($message)}
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p>{$message}</p>
    </div>
  </section>
{/if}
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="{base_url()}_admin/member/create">
        <div class="box-body">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="" required/>
                  {form_error('name')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">email</label>
                  <input type="email" class="form-control" name="email" placeholder="Email aktif" value="" required/>
                  {form_error('email')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Jenis member</label>
                  <select class="form-control" name="jenis" required/>
                    <option>--- Jenis member ---</option>
                    {foreach from=$get_member_tipe item=$row}
                      {if {$row->id} == $get_member->id_member}
                        <option value="{$row->id}" selected>{$row->name}</option>
                      {else}
                        <option value="{$row->id}">{$row->name}</option>
                      {/if}
                    {/foreach}
                  </select>
                  {form_error('jenis')}
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="password" class="form-control" name="password" value="" required/>
                  {form_error('password')}
                </div>
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Confirm Password</label>
                  <input type="password" class="form-control" name="confirm_password" value="" required/>
                  {form_error('confirm_password')}
                </div>
              </div>
            </div>
          
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
{/block}