<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model {

  public $table = 'users';
  
  public function __construct()
  {
    parent::__construct();
  }

  public function find_where($where)
  {
    $this->db->order_by('id', 'DESC');
    return $this->db->get_where($this->table, $where)->result();
  }
  public function delete($id)
  {
    $this->db->where('id_user', $id);
    $this->db->delete($this->table);
    return $this->db->affected_rows();
  }
  public function find_user_join_member($id)
  {
    $query = $this->db->where('id', $id)->limit(1)->get('member')->row();
    return $query->name;
  }
  public function find_type_member_id($where = array('akses' => 0))
  {
    $query = $this->db->where($where)->get('member')->result();
    return $query;
  }
}