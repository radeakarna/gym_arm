<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presence extends CI_Model {

  public $table = 'presences';
  
  public function __construct()
  {
    parent::__construct();
  }

  public function find_join_user_personal()
  {
    $this->db->select('*');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = presences.id_user');
    $this->db->where(array('users.id_member' => 2));
    $query = $this->db->get();
    return $query->result();
  }

  public function find_where_join_user_personal($id_user)
  {
    $this->db->select('*');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = presences.id_user');
    $this->db->where(array('users.id_member' => 2, 'presences.id_user' => $id_user));
    $query = $this->db->get();
    return $query->result();
  }

  public function find_join_user_personal_trainer()
  {
    $this->db->select('*');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = presences.id_user');
    $this->db->where(array('users.id_member' => 3));
    $query = $this->db->get();
    return $query->result();
  }

  public function find_where_join_user_personal_trainer($id_user)
  {
    $this->db->select('*');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = presences.id_user');
    $this->db->where(array('users.id_member' => 3, 'presences.id_user' => $id_user));
    $query = $this->db->get();
    return $query->result();
  }

  public function find_personal_member_by_id_member($id)
  {
    $this->db->select('users.id_member');    
    $this->db->from('users');
    $this->db->where(array('users.id' => $id, 'users.id_member' => 2));
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function find_personal_trainer_by_id_member($id)
  {
    $this->db->select('users.id_member');    
    $this->db->from('users');
    $this->db->where(array('users.id' => $id, 'users.id_member' => 3));
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function insert($data)
  {
    $this->db->set($data);
    $this->db->insert($this->table);
    return $this->db->insert_id();
  }
  
  public function delete($id)
  {
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    return $this->db->affected_rows();
  }
}