<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Model {

  public $table = 'payments';
  
  public function __construct()
  {
    parent::__construct();
  }

  public function find()
  {
    $this->db->order_by('id', 'DESC');
    return $this->db->get($this->table)->result();
  }
  
  public function find_by_id($id)
  {
    return $this->db->where('id', $id)->limit(1)->get($this->table)->row();
  }

  public function find_join_user_by_id_member($id)
  {
    $this->db->select('*');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', $id);
    $query = $this->db->get();
    return $query->result();
  }

  public function find_member_by_individu_date($id)
  {
    $this->db->select('users.id_member');    
    $this->db->from('users');
    // $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where(array('users.id' => $id, 'users.id_member' => 1));
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function find_member_by_personal_date($id)
  {
    $this->db->select('users.id_member');    
    $this->db->from('users');
    // $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where(array('users.id' => $id, 'users.id_member' => 2));
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function find_member_by_personal_trainer_date($id)
  {
    $this->db->select('users.id_member');    
    $this->db->from('users');
    // $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where(array('users.id' => $id, 'users.id_member' => 3));
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function insert($data)
  {
    $this->db->set($data);
    $this->db->insert($this->table);
    return $this->db->insert_id();
  }

  public function update($id, $data)
  {
    $this->db->where('id', $id);
    $this->db->set($data);
    $this->db->update($this->table);
    return $this->db->affected_rows();
  }

  public function update_by_id_user($id, $data)
  {
    $this->db->where('id_user', $id);
    $this->db->set($data);
    $this->db->update($this->table);
    return $this->db->affected_rows();
  }
  
  public function delete($id)
  {
    $this->db->where('id', $id);
    $this->db->delete($this->table);
    return $this->db->affected_rows();
  }
}