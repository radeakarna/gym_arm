<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chart extends CI_Model {

  public $table = 'payments';
  
  public function __construct()
  {
    parent::__construct();
  }

  public function individu_date_payment_day()
  {
    $this->db->select('individu_payments.create_at');    
    $this->db->from('individu_payments');
    $this->db->where('MONTH(create_at)', date('m')); // Tambahkan where bulan
    $this->db->where('YEAR(create_at)', date('Y')); // Tambahkan where tahun
    $this->db->group_by('DATE(create_at)');
    $this->db->limit(10);
    $this->db->order_by('individu_payments.create_at', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }
  public function individu_count_payment($date)
  {
    $this->db->select_sum('cost');
    return $this->db->where('DATE(create_at)', date('Y-m-d', strtotime($date)))->limit(1)->get('individu_payments')->row(); 
  }

  public function individu_date_payment_month()
  {
    $this->db->select('individu_payments.create_at');    
    $this->db->from('individu_payments');
    $this->db->where('YEAR(create_at)', date('Y')); // Tambahkan where tahun
    $this->db->group_by('MONTH(create_at)');
    $this->db->limit(10);
    $this->db->order_by('individu_payments.create_at', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }
  public function individu_count_payment_month($date)
  {
    $this->db->select_sum('cost');
    return $this->db->where('MONTH(create_at)', date('m', strtotime($date)))->limit(1)->get('individu_payments')->row(); 
  }

  public function individu_date_payment_year()
  {
    $this->db->select('individu_payments.create_at');    
    $this->db->from('individu_payments');
    $this->db->group_by('YEAR(create_at)');
    $this->db->limit(10);
    $this->db->order_by('individu_payments.create_at', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }
  public function individu_count_payment_year($date)
  {
    $this->db->select_sum('cost');
    return $this->db->where('YEAR(create_at)', date('Y', strtotime($date)))->limit(1)->get('individu_payments')->row(); 
  }

  public function personal_date_payment_day()
  {
    $this->db->select('payments.date_payment');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 2);
    $this->db->where('MONTH(payments.date_payment)', date('m')); // Tambahkan where bulan
    $this->db->where('YEAR(payments.date_payment)', date('Y')); // Tambahkan where tahun
    $this->db->group_by('DATE(date_payment)');
    $this->db->limit(10);
    $this->db->order_by('payments.date_payment', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }
  public function personal_count_payment($date)
  {
    $this->db->select_sum('payments.cost');
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 2);
    $this->db->where('DATE(date_payment)', date('Y-m-d', strtotime($date)));
    $query = $this->db->get()->row();
    return $query;  
  }

  public function personal_date_payment_month()
  {
    $this->db->select('payments.date_payment');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 2);
    $this->db->where('YEAR(payments.date_payment)', date('Y')); // Tambahkan where tahun
    $this->db->group_by('MONTH(date_payment)');
    $this->db->limit(10);
    $this->db->order_by('payments.date_payment', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }
  public function personal_count_payment_month($date)
  {
    $this->db->select_sum('payments.cost');
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 2);
    $this->db->where('MONTH(date_payment)', date('m', strtotime($date)));
    $query = $this->db->get()->row();
    return $query;  
  }

  public function personal_date_payment_year()
  {
    $this->db->select('payments.date_payment');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 2);
    $this->db->group_by('YEAR(date_payment)');
    $this->db->limit(10);
    $this->db->order_by('payments.date_payment', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }
  public function personal_count_payment_year($date)
  {
    $this->db->select_sum('payments.cost');
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 2);
    $this->db->where('YEAR(date_payment)', date('Y', strtotime($date)));
    $query = $this->db->get()->row();
    return $query;  
  }

  // Personal trainer

  public function personal_trainer_date_payment_day()
  {
    $this->db->select('payments.date_payment');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 3);
    $this->db->where('MONTH(payments.date_payment)', date('m')); // Tambahkan where bulan
    $this->db->where('YEAR(payments.date_payment)', date('Y')); // Tambahkan where tahun
    $this->db->group_by('DATE(date_payment)');
    $this->db->limit(10);
    $this->db->order_by('payments.date_payment', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }
  public function personal_trainer_count_payment($date)
  {
    $this->db->select_sum('payments.cost');
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 3);
    $this->db->where('DATE(date_payment)', date('Y-m-d', strtotime($date)));
    $query = $this->db->get()->row();
    return $query;  
  }

  public function personal_trainer_date_payment_month()
  {
    $this->db->select('payments.date_payment');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 3);
    $this->db->where('YEAR(payments.date_payment)', date('Y')); // Tambahkan where tahun
    $this->db->group_by('MONTH(date_payment)');
    $this->db->limit(10);
    $this->db->order_by('payments.date_payment', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }
  public function personal_trainer_count_payment_month($date)
  {
    $this->db->select_sum('payments.cost');
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 3);
    $this->db->where('MONTH(date_payment)', date('m', strtotime($date)));
    $query = $this->db->get()->row();
    return $query;  
  }

  public function personal_trainer_date_payment_year()
  {
    $this->db->select('payments.date_payment');    
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 3);
    $this->db->group_by('YEAR(date_payment)');
    $this->db->limit(10);
    $this->db->order_by('payments.date_payment', 'DESC');
    $query = $this->db->get();
    return $query->result();
  }
  public function personal_trainer_count_payment_year($date)
  {
    $this->db->select_sum('payments.cost');
    $this->db->from('users');
    $this->db->join($this->table, 'users.id = payments.id_user');
    $this->db->where('users.id_member', 3);
    $this->db->where('YEAR(date_payment)', date('Y', strtotime($date)));
    $query = $this->db->get()->row();
    return $query;  
  }
}