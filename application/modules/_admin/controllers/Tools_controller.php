<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools_controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('base','member');
        $this->member->table = 'tools';   
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){
        $data = array(
                'title' => 'Admin || WEB',
                'username' => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
            );
        $this->parser->parse("tools/index.tpl",$data);
    }
    public function show()
    {
        $list = $this->member->find('users');
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $no++;
            $row['no']         = $no;
            $row['name']       = $k->name;
            $row['username']   = $k->username;
            $row['created_at'] = $k->created_at;
            $row['action']     = "<center>
                                        <span class='btn btn-danger btn-flat btn-sm' data-toggle='tooltip' onclick='hpsdt(".'"'.$k->id.'"'.")' title='hapus data'><i class='fa fa-trash-o'></i></span>
                                        <a href='".base_url('_admin/berita/editdata/'.$k->id)."'><span class='btn btn-info btn-flat btn-sm' data-toggle='tooltip' title='edit data ".$k->username."'><i class='fa fa-edit'></i></span></a>
                                    </center>";
            $data[]=$row;
        }
        $output = array("data" => $data,);
        echo json_encode($output);
    }
    public function create()
    {
        $data = array(
                'title' => 'Admin || WEB',
                'username' => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
            );
        $this->parser->parse("tools/create.tpl",$data);
    }
    public function store()
    {
        $json = array();
        $this->form_validation->set_rules('name', 'name', 'required|is_unique[tools.name]');
        $this->form_validation->set_rules('jumlah', 'jumlah alat', 'trim|required|min_length[1]');
        $this->form_validation->set_rules('deskripsi', 'Keterangan', 'required');
        if (!$this->form_validation->run()) {
            $this->create();
        }else{
            $data = array(
                'name'             => $this->input->post('name'),
                'description'         => $this->input->post('deskripsi'),
                'count'         => $this->input->post('jumlah'),
            );
            $this->member->insert($data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('_admin/tool/create'),'refresh');
        }
    }
    
}
