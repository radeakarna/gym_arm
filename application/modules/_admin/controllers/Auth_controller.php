<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_controller extends MX_Controller  {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth','auth');
    }

    public function index(){
        if($this->auth->is_logged_in() == true){
            $data = array(
                'token'            => $this->security->get_csrf_token_name(),
                'value'            => $this->security->get_csrf_hash(),
                );
            $this->parser->parse("auth/index.tpl", $data);
        }else{
            redirect(base_url("_admin/"));
            
        }
    }
    public function find(){
        $this->form_validation->set_rules('username','username','required');
        $this->form_validation->set_rules('password','password','required');
        

        if($this->form_validation->run() != false){
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $uname = array('username' => $username);
            $cek_user = $this->auth->find($uname)->num_rows();
            if ($cek_user > 0) {
                $upass = array('password' => md5($password));
                $cek_upass = $this->auth->find($upass)->num_rows();
                if ($cek_upass > 0) {
                    $where = array(
                        'username' => $username,
                        'password' => md5($password),
                        'role' => 0,
                        );
                    $cek = $this->auth->find($where)->num_rows();
                    $cekdua = $this->auth->find($where);
                    if($cek == 1){
                            $data_session = array(
                            'username' =>  $cekdua->row()->username,
                            'id_login' =>  $cekdua->row()->id,
                            //'token' =>  $this->input->post('csrf_test_name'),
                            'status' => "loginadmin",
                            );
                            $this->session->set_userdata($data_session);
                            $data = $this->session->userdata;
                            redirect(base_url("_admin/"));    
                    }else{
                        echo "<script>alert('Anda Gagal Login!');window.history.go(-1);</script>";
                    }
                }else{
                    echo "<script>alert('password salah');window.history.go(-1);</script>";
                }
            }else{
                echo "<script>alert('Username salah');window.history.go(-1);</script>";
            }
        }else{
            redirect(base_url("_admin/auth/"));
        }
        
                  
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('_admin/auth/', 'refresh');
    }
    
}
