<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal_presences_controller extends MX_Controller  {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('presence','presence');
        $this->load->model('member','user');
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){
        $token = $this->get_token();
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'token_two' => $token['token_name'],
                'value_two' => $token['token_value'],
                'message'   => $this->session->userdata('message'),
            );
        $this->parser->parse("presensi/personal/index.tpl",$data);
    }

    public function presence()
    {
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'id_member' => $this->uri->segment(4),
                'message'   => $this->session->userdata('message'),
            );
        $this->parser->parse("presensi/personal/precence.tpl",$data);
    }

    public function get_token()
    {
        $data = array(
            'token_name' => $this->security->get_csrf_token_name(), 
            'token_value' => $this->security->get_csrf_hash(), 
            );

        return $data;
    }

    public function show(){
        $id = $this->input->post('id_member');
        $list = $this->presence->find_where_join_user_personal($id);
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $no++;
            $row['no']         = $no;
            $row['name']       = $k->name;
            $row['token_value']= $this->security->get_csrf_hash();
            $row['code']       = $k->id_user;
            $row['created_at'] = tgl_indo($k->created_at);
            $row['action']     = "<center>
                                        <span class='btn btn-danger btn-flat btn-sm' data-toggle='tooltip' onclick='destroy(".'"'.$k->id.'"'.",".'"'.base_url('_admin/personal_presences/destroy').'"'.", ".'"'.$this->security->get_csrf_hash().'"'.")' title='hapus data'><i class='fa fa-trash-o'></i></span>
                                    </center>";
            $data[]=$row;
        }
        $output = array("data" => $data,);
        echo json_encode($output);
    }

    public function show_member(){
        $list = $this->user->find_where(array('status' =>'user', 'role' => 2));
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $no++;
            $row['no']         = $no;
            $row['id']         = $k->id;
            $row['name']       = $k->name;
            $row['type']       = $this->user->find_user_join_member($k->id_member);
            $row['created_at'] = tgl_indo($k->created_at);
            $row['action']     = "<center>
                                        <a href='".base_url('_admin/personal_presences/presence/'.$k->id)."'><span class='btn btn-success btn-flat btn-sm' data-toggle='tooltip' title='Lihat presensi ".$k->username."'><i class='fa fa-eye'></i></span></a>
                                    </center>";
            $data[]=$row;
        }
        $output = array("data" => $data,);
        echo json_encode($output);
    }
    public function create()
    {
        $this->form_validation->set_rules('id_member', 'id_member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $data = array(
                    'title'     => 'Admin || tambah data member',
                    'username'  => $this->session->userdata('username'),
                    'token'     => $this->security->get_csrf_token_name(),
                    'value'     => $this->security->get_csrf_hash(),
                    'message' => $this->session->userdata('message'),
                );
            $this->parser->parse("presensi/personal/create.tpl",$data);
        }else{
            $this->store();
        }
    }
    public function store()
    {
        $this->form_validation->set_rules('id_member', 'id_member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            if ($this->presence->find_personal_member_by_id_member($this->input->post('id_member')) <= 0) {
                $this->session->set_flashdata('message', 'Bukan termasuk member personal');
                redirect(base_url('_admin/personal_presences'),'refresh');
            }
            $data = array(
                'id_user'       => $this->input->post('id_member'),
            );
            $this->presence->insert($data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('_admin/personal_presences'),'refresh');
        }
    }
    
    public function destroy()
    {
        $id = $this->input->post('id');
        $this->presence->delete($id);
        echo json_encode(array('status' => TRUE, 'token' => $this->security->get_csrf_hash()));
    }
    
}
