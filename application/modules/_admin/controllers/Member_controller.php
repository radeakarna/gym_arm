<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_controller extends MX_Controller  {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('base','member');
        $this->load->model('member','user');
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message'   => $this->session->userdata('message'),
            );
        $this->parser->parse("member/index.tpl",$data);
    }
    public function show()
    {
        $list = $this->user->find_where(array('status' =>'user'));
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $no++;
            $row['no']         = $no;
            $row['name']       = $k->name;
            $row['type']       = $this->user->find_user_join_member($k->id_member);
            $row['username']   = $k->username;
            $row['created_at'] = tgl_indo($k->created_at);
            $row['lost_member']= "<span class='btn btn-success btn-flat btn-sm' data-toggle='tooltip' onclick='continou(".'"'.$k->id.'"'.",".'"'.base_url('_admin/member_controller/continue_member').'"'.", ".'"'.$this->security->get_csrf_hash().'"'.")' title='Perpanjang 25 hari member ".$k->username."'>".tgl_indo($k->lost_member)."</span>";
            $row['action']     = "<center>
                                        <span class='btn btn-danger btn-flat btn-sm' data-toggle='tooltip' onclick='destroy(".'"'.$k->id.'"'.",".'"'.base_url('_admin/member/destroy').'"'.", ".'"'.$this->security->get_csrf_hash().'"'.")' title='hapus data'><i class='fa fa-trash-o'></i></span>
                                        <a href='".base_url('_admin/member/edit/'.$k->id)."'><span class='btn btn-info btn-flat btn-sm' data-toggle='tooltip' title='edit data ".$k->username."'><i class='fa fa-edit'></i></span></a>
                                    </center>";
            $data[]=$row;
        }
        $output = array("data" => $data,);
        echo json_encode($output);
    }
    public function create(){
        $json = array();
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $data = array(
                    'title'     => 'Admin || tambah data member',
                    'username'  => $this->session->userdata('username'),
                    'token'     => $this->security->get_csrf_token_name(),
                    'value'     => $this->security->get_csrf_hash(),
                    'message' => $this->session->userdata('message'),
                    'get_member_tipe'=> $this->user->find_type_member_id(),
                );
            $this->parser->parse("member/create.tpl",$data);
        }else{
            $this->store();
        }
    }
    public function store()
    {
        $json = array();
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('jenis', 'Jenis member', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'name'             => $this->input->post('name'),
                'id_member'             => $this->input->post('jenis'),
                'username'         => $this->input->post('email'),
                'password'         => md5($this->input->post('password')),
                'status'           => 'user',
                'role'             => 1,
                'lost_member'      => add_date(date('Y-m-d'),25),
            );
            $this->member->insert($data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('_admin/member/create'),'refresh');
        }
    }
    public function destroy()
    {
        $id = $this->input->post('id');
        $this->member->delete($id);
        $this->user->table= 'persons_data';
        $this->user->delete($id);
        echo json_encode(array('status' => TRUE, 'token' => $this->security->get_csrf_hash()));
    }

    public function continue_member()
    {
        $id = $this->input->post('id');
        $get_member = $this->member->find_by_id($id);
        $data = array(
            'lost_member'  => add_date($get_member->lost_member,30),
        );
        $this->member->update($id, $data);
        echo json_encode(array('status' => TRUE, 'token' => $this->security->get_csrf_hash()));
    }

    public function edit(){
        $id = $this->uri->segment(4);
        $data = array(
                'title'     => 'Edit member || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message'   => $this->session->userdata('message'),
                'get_member'=> $this->member->find_by_id($id),
                'get_member_tipe'=> $this->user->find_type_member_id(),
            );
        $this->parser->parse("member/edit.tpl",$data);
    }

    public function update()
    {
        $id = $this->uri->segment(4);
        if (!empty($this->input->post('password'))) {
            $this->form_validation->set_rules('name', 'name', 'required');
            //$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.username]');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
            $this->form_validation->set_rules('jenis', 'Jenis member', 'required');
            $this->form_validation->set_rules('akun', 'akun', 'required');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
            $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
            if (!$this->form_validation->run()) {
                redirect_back();
            }else{
                $data = array(
                    'name'             => $this->input->post('name'),
                    'id_member'        => $this->input->post('jenis'),
                    //'username'         => $this->input->post('email'),
                    'password'         => md5($this->input->post('password')),
                    'lost_member'      => $this->input->post('akun'),
                );
                $this->member->update($id, $data);
                $this->session->set_flashdata('message', 'Data berhasil diperbarui');
                redirect(base_url('_admin/member/'),'refresh');
            }
        }else{
            $this->form_validation->set_rules('name', 'name', 'required');
            //$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.username]');
            $this->form_validation->set_rules('jenis', 'Jenis member', 'required');
            $this->form_validation->set_rules('akun', 'akun', 'required');
            $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
            if (!$this->form_validation->run()) {
                redirect_back();
            }else{
                $data = array(
                    'name'             => $this->input->post('name'),
                    'id_member'        => $this->input->post('jenis'),
                    'lost_member'      => $this->input->post('akun'),
                    //'username'         => $this->input->post('email'),
                );
                $this->member->update($id, $data);
                $this->session->set_flashdata('message', 'Data berhasil diperbarui');
                redirect(base_url('_admin/member/'),'refresh');
            }
        }
    }
    
}
