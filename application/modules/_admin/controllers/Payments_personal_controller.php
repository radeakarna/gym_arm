<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments_personal_controller extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment','member');
        $this->member->table = 'payments';
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
        $this->parser->parse("pembayaran/personal/index.tpl",$data);
    }
    public function show(){
        $list = $this->member->find_join_user_by_id_member(2);
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $no++;
            $row['no']         = $no;
            $row['name']       = $k->name;
            $row['code']       = $k->id_user;
            $row['cost']       = $k->cost;
            $row['created_at'] = tgl_indo($k->date_payment);
            $row['action']     = "<center>
                                        <span class='btn btn-danger btn-flat btn-sm' data-toggle='tooltip' onclick='destroy(".'"'.$k->id.'"'.",".'"'.base_url('_admin/payment_personal/destroy').'"'.", ".'"'.$this->security->get_csrf_hash().'"'.")' title='hapus data'><i class='fa fa-trash-o'></i></span>
                                    </center>";
            $data[]=$row;
        }
        $output = array("data" => $data,);
        echo json_encode($output);
    }
    public function create(){
        $this->form_validation->set_rules('id_member', 'id_member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $data = array(
                    'title'     => 'Admin || tambah data member',
                    'username'  => $this->session->userdata('username'),
                    'token'     => $this->security->get_csrf_token_name(),
                    'value'     => $this->security->get_csrf_hash(),
                    'message' => $this->session->userdata('message'),
                );
            $this->parser->parse("pembayaran/personal/create.tpl",$data);
        }else{
            $this->store();
        }
    }
    public function store()
    {
        $this->form_validation->set_rules('id_member', 'id_member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            if ($this->member->find_member_by_personal_date($this->input->post('id_member')) <= 0) {
                $this->session->set_flashdata('message', 'Bukan termasuk member personal');
                redirect(base_url('_admin/payment_personal/create'),'refresh');
            }
            $data = array(
                'id_user'       => $this->input->post('id_member'),
                'cost'          => get_metsos('personal'),
                'date_payment'  => date('Y-m-d'),
            );
            $this->member->insert($data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('_admin/payment_personal/create'),'refresh');
        }
    }
    public function destroy()
    {
        $id = $this->input->post('id');
        $this->member->delete($id);
        echo json_encode(array('status' => TRUE, 'token' => $this->security->get_csrf_hash()));
    }
    
}
