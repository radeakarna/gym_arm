<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments_individu_controller extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('payment','member');
        // $this->load->model('base','user');
        $this->load->model('individu_payment','payment');
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){    
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
        $this->parser->parse("pembayaran/individu/index.tpl",$data);
    }
    public function show(){
        // $list = $this->member->find_join_user_by_id_member(1);
        // $data = array();
        // $no = 0;
        // foreach ($list as $k) {
        //     $row = array();
        //     $no++;
        //     $row['no']         = $no;
        //     $row['name']       = $k->name;
        //     $row['code']       = $k->id_user;
        //     $row['cost']       = $k->cost;
        //     $row['created_at'] = tgl_indo($k->date_payment);
        //     $row['action']     = "<center>
        //                                 <span class='btn btn-danger btn-flat btn-sm' data-toggle='tooltip' onclick='destroy(".'"'.$k->id.'"'.",".'"'.base_url('_admin/payment_individu/destroy').'"'.", ".'"'.$this->security->get_csrf_hash().'"'.")' title='hapus data'><i class='fa fa-trash-o'></i></span>
        //                             </center>";
        //     $data[]=$row;
        // }
        // $output = array("data" => $data,);
        // echo json_encode($output);

        $list = $this->payment->find();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $no++;
            $row['no']         = $no;
            $row['name']       = $k->name;
            $row['cost']       = $k->cost;
            $row['created_at'] = tgl_indo($k->create_at);
            $row['action']     = "<center>
                                        <span class='btn btn-danger btn-flat btn-sm' data-toggle='tooltip' onclick='destroy(".'"'.$k->id.'"'.",".'"'.base_url('_admin/payment_individu/destroy').'"'.", ".'"'.$this->security->get_csrf_hash().'"'.")' title='hapus data'><i class='fa fa-trash-o'></i></span>
                                    </center>";
            $data[]=$row;
        }
        $output = array("data" => $data,);
        echo json_encode($output);
    }
    public function create(){
        $this->form_validation->set_rules('id_member', 'id_member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $data = array(
                    'title'     => 'Admin || tambah data member',
                    'username'  => $this->session->userdata('username'),
                    'token'     => $this->security->get_csrf_token_name(),
                    'value'     => $this->security->get_csrf_hash(),
                    'message' => $this->session->userdata('message'),
                );
            $this->parser->parse("pembayaran/individu/create.tpl",$data);
        }else{
            $this->store();
        }
    }
    public function store()
    {
        $this->form_validation->set_rules('id_member', 'id_member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            // if ($this->member->find_member_by_individu_date($this->input->post('id_member')) <= 0) {
            //     $this->session->set_flashdata('message', 'Bukan termasuk member individu');
            //     redirect(base_url('_admin/payment_individu/create'),'refresh');
            // }
            $data = array(
                'name'       => $this->input->post('id_member'),
                'cost'          => get_metsos('member_day'),
            );
            $this->payment->insert($data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('_admin/payment_individu/create'),'refresh');
        }
    }
    public function destroy()
    {
        $id = $this->input->post('id');
        $this->payment->delete($id);
        echo json_encode(array('status' => TRUE, 'token' => $this->security->get_csrf_hash()));
    }
    public function find_user()
    {
        $id = $this->input->post('id_member');
        $get_user = $this->user->find_by_id($id);
        $data = array();
        $data['name']  =$get_user->name;
        echo json_encode(array('data' => $data));
    }
    
}
