<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Individu_controller extends MX_Controller  {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('base','member');
        $this->load->model('member','user');
        $this->load->model('self_data');
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
        $this->parser->parse("member/individu/index.tpl",$data);
    }
    public function show()
    {
        $list = $this->user->find_where(array('status' =>'user', 'role' => 3));
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $no++;
            $row['no']         = $no;
            $row['id']         = $k->id;
            $row['name']       = $k->name;
            $row['type']       = $this->user->find_user_join_member($k->id_member);
            $row['created_at'] = tgl_indo($k->created_at);
            $row['action']     = "<center>
                                        <span class='btn btn-danger btn-flat btn-sm' data-toggle='tooltip' onclick='destroy(".'"'.$k->id.'"'.",".'"'.base_url('_admin/member/destroy').'"'.", ".'"'.$this->security->get_csrf_hash().'"'.")' title='hapus data'><i class='fa fa-trash-o'></i></span>
                                        <a href='".base_url('_admin/individu/edit/'.$k->id)."'><span class='btn btn-info btn-flat btn-sm' data-toggle='tooltip' title='edit data ".$k->username."'><i class='fa fa-edit'></i></span></a>
                                    </center>";
            $data[]=$row;
        }
        $output = array("data" => $data,);
        echo json_encode($output);
    }
    public function create(){
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('nik', 'Nomor KTP', 'required|min_length[8]');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat lahir', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'required');
        $this->form_validation->set_rules('no_telepon', 'Nomor telepon', 'required');
        $this->form_validation->set_rules('jk', 'name', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $data = array(
                    'title'     => 'Admin || tambah data member',
                    'username'  => $this->session->userdata('username'),
                    'token'     => $this->security->get_csrf_token_name(),
                    'value'     => $this->security->get_csrf_hash(),
                    'message' => $this->session->userdata('message'),
                    'get_member_tipe'=> $this->user->find_type_member_id(),
                );
            $this->parser->parse("member/individu/create.tpl",$data);
        }else{
            $this->store();
        }
    }
    public function store()
    {
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('nik', 'Nomor KTP', 'required|min_length[8]');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat lahir', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'required');
        $this->form_validation->set_rules('no_telepon', 'Nomor telepon', 'required');
        $this->form_validation->set_rules('jk', 'name', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'name'             => $this->input->post('name'),
                'id_member'        => 1,
                'status'           => 'user',
                'role'             => 3,
                'lost_member'      => add_date(date('Y-m-d'),1),
            );
            $this->member->insert($data);
            $self_data = array(
                'id_user'         => $this->db->insert_id(),
                'nisn'            => $this->input->post('nik'),
                'sex'             => $this->input->post('jk'),
                'telp'            => $this->input->post('no_telepon'),
                'addres'          => $this->input->post('alamat'),
                'place_of_birth'  => $this->input->post('tmp_lahir'),
                'date_of_birth'   => $this->input->post('tgl_lahir'),
            );
            $this->self_data->insert($self_data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('_admin/individu/create'),'refresh');
        }
    }
    public function destroy()
    {
        $id = $this->input->post('id');
        $this->member->delete($id);
        $this->user->table= 'persons_data';
        $this->user->delete($id);
        echo json_encode(array('status' => TRUE, 'token' => $this->security->get_csrf_hash()));
    }

    public function continue_member()
    {
        $id = $this->input->post('id');
        $get_member = $this->member->find_by_id($id);
        $data = array(
            'lost_member'  => add_date($get_member->lost_member,25),
        );
        $this->member->update($id, $data);
        echo json_encode(array('status' => TRUE, 'token' => $this->security->get_csrf_hash()));
    }

    public function edit(){
        $id = $this->uri->segment(4);
        $data = array(
                'title'     => 'Edit member || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message'   => $this->session->userdata('message'),
                'get_member'=> $this->self_data->find_join_user_by_id($id),
            );
        $this->parser->parse("member/individu/edit.tpl",$data);
    }

    public function update()
    {
        $id = $this->uri->segment(4);
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('nik', 'Nomor KTP', 'required|min_length[8]');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat lahir', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'required');
        $this->form_validation->set_rules('no_telepon', 'Nomor telepon', 'required');
        $this->form_validation->set_rules('jk', 'name', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $this->edit();
        }else{
            $data = array(
                'name'             => $this->input->post('name'),
            );
            $this->member->update($id, $data);
            $self_data = array(
                'nisn'            => $this->input->post('nik'),
                'sex'             => $this->input->post('jk'),
                'telp'            => $this->input->post('no_telepon'),
                'addres'          => $this->input->post('alamat'),
                'place_of_birth'  => $this->input->post('tmp_lahir'),
                'date_of_birth'   => $this->input->post('tgl_lahir'),
            );
            $this->self_data->update_by_id_user($id, $self_data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/individu/'),'refresh');
        }
    }
    
}
