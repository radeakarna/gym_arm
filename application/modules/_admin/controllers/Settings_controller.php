<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_controller extends MX_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('base','member');
        $this->member->table = 'settings';
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){
        $data = array(
                'title'     => 'Admin settings || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
        $this->parser->parse("settings/medsos.tpl",$data);
    }

    public function about(){
        $data = array(
                'title'     => 'Admin settings || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
        $this->parser->parse("settings/about.tpl",$data);
    }

    public function cost(){
        $data = array(
                'title'     => 'Admin settings || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
        $this->parser->parse("settings/biaya.tpl",$data);
    }

    public function update_facebook(){
        $id = 1;
        $this->form_validation->set_rules('facebook', 'name', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('facebook'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/'),'refresh');
        }
    }

    public function update_instagram(){
        $id = 2;
        $this->form_validation->set_rules('instragram', 'name', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('instragram'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/'),'refresh');
        }
    }

    public function update_twitter(){
        $id = 3;
        $this->form_validation->set_rules('twitter', 'name', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('twitter'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/'),'refresh');
        }
    }

    public function update_gmail(){
        $id = 4;
        $this->form_validation->set_rules('gmail', 'gmail', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('gmail'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/'),'refresh');
        }
    }

    public function update_contact(){
        $id = 5;
        $this->form_validation->set_rules('contact', 'contact', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('contact'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/'),'refresh');
        }
    }

    public function update_addres(){
        $id = 6;
        $this->form_validation->set_rules('addres', 'contact', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('addres'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/'),'refresh');
        }
    }

    public function update_about(){
        $id = 7;
        $this->form_validation->set_rules('about', 'about', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('about'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/about'),'refresh');
        }
    }

    public function update_cost_personal(){
        $id = 8;
        $this->form_validation->set_rules('member', 'member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('member'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/cost'),'refresh');
        }
    }

    public function update_cost_personal_trainer(){
        $id = 9;
        $this->form_validation->set_rules('member', 'member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('member'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/cost'),'refresh');
        }
    }

    public function update_cost_member_day(){
        $id = 10;
        $this->form_validation->set_rules('member', 'member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'value'      => $this->input->post('member'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/setting/cost'),'refresh');
        }
    }
    
}
