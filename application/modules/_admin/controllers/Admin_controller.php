<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends MX_Controller  {

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('modalmain','m_main');
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }
    public function index(){
        $data = array(
                'title' => 'Admin || WEB',
                'username' => $this->session->userdata('username'),
            );
        $this->parser->parse("index.tpl",$data);
    }
    
}
