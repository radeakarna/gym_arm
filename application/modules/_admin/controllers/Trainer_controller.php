<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trainer_controller extends MX_Controller  {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('trainer');
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
        $this->parser->parse("trainer/index.tpl",$data);
    }
    public function show()
    {
        $list = $this->trainer->find();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $no++;
            $row['no']         = $no;
            $row['id']         = $k->id;
            $row['nama']       = $k->nama;
            $row['alamat']     = $k->alamat;
            $row['foto']       = '<img src="'.base_url("assets/user/images/trainer/".$k->foto).'" width="30%" class="image_responsive" >';
            $row['jk']         = $k->jk;
            $row['action']     = "<center>
                                        <span class='btn btn-danger btn-flat btn-sm' data-toggle='tooltip' onclick='destroy(".'"'.$k->id.'"'.",".'"'.base_url('_admin/trainer/destroy').'"'.", ".'"'.$this->security->get_csrf_hash().'"'.")' title='hapus data'><i class='fa fa-trash-o'></i></span>
                                        <a href='".base_url('_admin/trainer/edit/'.$k->id)."'><span class='btn btn-info btn-flat btn-sm' data-toggle='tooltip' title='edit data ".$k->nama."'><i class='fa fa-edit'></i></span></a>
                                    </center>";
            $data[]=$row;
        }
        $output = array("data" => $data,);
        echo json_encode($output);
    }
    public function create(){
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('jk', 'jenis kelamin', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $data = array(
                    'title'     => 'Admin || tambah data member',
                    'username'  => $this->session->userdata('username'),
                    'token'     => $this->security->get_csrf_token_name(),
                    'value'     => $this->security->get_csrf_hash(),
                    'message' => $this->session->userdata('message'),
                );
            $this->parser->parse("trainer/create.tpl",$data);
        }else{
            $this->store();
        }
    }
    public function store()
    {
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('jk', 'name', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $config=[
                    // 'file_name'     => date('HisdfY').$_FILES['foto']['name'],
                    'upload_path'   => './assets/user/images/trainer/',
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => 10000,
                    'encrypt_name'  => TRUE,
                ];
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('foto')){
                print_r($this->upload->display_errors());
                //echo json_encode(array("status" => FALSE));
            }else{
                $this->session->set_flashdata('message', 'Data berhasil disimpan');
                $data = array(
                    'nama'           => $this->input->post('name'),
                    'foto'           => $this->upload->data('file_name'),
                    'alamat'         => $this->input->post('alamat'),
                    'jk'             => $this->input->post('jk'),
                );
                $this->trainer->insert($data);
                redirect(base_url('_admin/trainer/create'),'refresh');   
            }
        }
    }
    public function destroy()
    {
        $id = $this->input->post('id');
        $this->trainer->delete($id);
        echo json_encode(array('status' => TRUE, 'token' => $this->security->get_csrf_hash()));
    }

    public function edit(){
        $id = $this->uri->segment(4);
        $data = array(
                'title'     => 'Edit member || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message'   => $this->session->userdata('message'),
                'get_trainer'=> $this->trainer->find_by_id($id),
            );
        $this->parser->parse("trainer/edit.tpl",$data);
    }

    public function update()
    {
        $id = $this->uri->segment(4);
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('jk', 'name', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/trainer/edit/'.$id),'refresh');
        }else{
            if (!empty($_FILES['foto']['name'])) {
                $config=[
                    'upload_path'   => './assets/user/images/trainer/',
                    'allowed_types' => 'gif|jpg|png',
                    'max_size'      => 10000,
                    'encrypt_name'  => TRUE,
                ];
                $get_trainer = $this->trainer->find_by_id($id);
                unlink(FCPATH."assets/user/images/trainer/".$get_trainer->foto);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('foto')){
                    print_r($this->upload->display_errors());
                    
                }
                $data = array(
                    'nama'           => $this->input->post('name'),
                    'foto'           => $this->upload->data('file_name'),
                    'alamat'         => $this->input->post('alamat'),
                    'jk'             => $this->input->post('jk'),
                );
            }else{
                $data = array(
                    'nama'           => $this->input->post('name'),
                    'alamat'         => $this->input->post('alamat'),
                    'jk'             => $this->input->post('jk'),
                );
            }
            $this->trainer->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/trainer/'),'refresh');
        }
    }
    
}
