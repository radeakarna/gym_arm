<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chart_controller extends MX_Controller  {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('chart');
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message'   => $this->session->userdata('message'),
            );
        $this->parser->parse("chart/index.tpl",$data);
    }

    public function personal(){
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message'   => $this->session->userdata('message'),
            );
        $this->parser->parse("chart/personal.tpl",$data);
    }

    public function personal_trainer(){
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message'   => $this->session->userdata('message'),
            );
        $this->parser->parse("chart/personal_trainer.tpl",$data);
    }

    public function get_data_day()
    {
        $list = $this->chart->individu_date_payment_day();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $sum_cost = $this->chart->individu_count_payment(date('Y-m-d', strtotime($k->create_at)));
            $row['y'] = date('Y-m-d', strtotime($k->create_at));
            $row['a'] = $sum_cost->cost;
            $data[]=$row;
        }
        echo json_encode($data);
    }
    public function get_data_month()
    {
        $list = $this->chart->individu_date_payment_month();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $sum_cost = $this->chart->individu_count_payment_month($k->create_at);
            $row['y'] = date('Y F', strtotime($k->create_at));
            $row['a'] = $sum_cost->cost;
            $data[]=$row;
        }
        echo json_encode($data);
    }
    public function get_data_year()
    {
        $list = $this->chart->individu_date_payment_month();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $sum_cost = $this->chart->individu_count_payment_month($k->create_at);
            $row['y'] = date('Y', strtotime($k->create_at));
            $row['a'] = $sum_cost->cost;
            $data[]=$row;
        }
        echo json_encode($data);
    }

    public function get_personal_data_day()
    {
        $list = $this->chart->personal_date_payment_day();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $sum_cost = $this->chart->personal_count_payment(date('Y-m-d', strtotime($k->date_payment)));
            $row['y'] = date('Y-m-d', strtotime($k->date_payment));
            $row['a'] = $sum_cost->cost;
            $data[]=$row;
        }
        echo json_encode($data);
    }

    public function get_personal_data_month()
    {
        $list = $this->chart->personal_date_payment_month();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $sum_cost = $this->chart->personal_count_payment_month(date('Y-m-d', strtotime($k->date_payment)));
            $row['y'] = date('Y F', strtotime($k->date_payment));
            $row['a'] = $sum_cost->cost;
            $data[]=$row;
        }
        echo json_encode($data);
    }

    public function get_personal_data_year()
    {
        $list = $this->chart->personal_date_payment_year();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $sum_cost = $this->chart->personal_count_payment_year(date('Y-m-d', strtotime($k->date_payment)));
            $row['y'] = date('Y', strtotime($k->date_payment));
            $row['a'] = $sum_cost->cost;
            $data[]=$row;
        }
        echo json_encode($data);
    }

    public function get_personal_trainer_data_day()
    {
        $list = $this->chart->personal_trainer_date_payment_day();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $sum_cost = $this->chart->personal_trainer_count_payment(date('Y-m-d', strtotime($k->date_payment)));
            $row['y'] = date('Y-m-d', strtotime($k->date_payment));
            $row['a'] = $sum_cost->cost;
            $data[]=$row;
        }
        echo json_encode($data);
    }

    public function get_personal_trainer_data_month()
    {
        $list = $this->chart->personal_trainer_date_payment_month();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $sum_cost = $this->chart->personal_trainer_count_payment_month(date('Y-m-d', strtotime($k->date_payment)));
            $row['y'] = date('Y F', strtotime($k->date_payment));
            $row['a'] = $sum_cost->cost;
            $data[]=$row;
        }
        echo json_encode($data);
    }

    public function get_personal_trainer_data_year()
    {
        $list = $this->chart->personal_trainer_date_payment_year();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $sum_cost = $this->chart->personal_trainer_count_payment_year(date('Y-m-d', strtotime($k->date_payment)));
            $row['y'] = date('Y', strtotime($k->date_payment));
            $row['a'] = $sum_cost->cost;
            $data[]=$row;
        }
        echo json_encode($data);
    }
}
