<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments_Controller extends MX_Controller  {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('base','member');
        $this->member->table = 'payments';
        if($this->session->userdata('status') != "loginadmin" AND !$this->session->userdata('username')){
            redirect(base_url("_admin/auth/"));
        }
    }

    public function index(){
        $data = array(
                'title'     => 'Admin || WEB',
                'username'  => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
        $this->parser->parse("pembayaran/index.tpl",$data);
    }
    public function show(){
        $list = $this->member->find();
        $data = array();
        $no = 0;
        foreach ($list as $k) {
            $row = array();
            $no++;
            $row['no']         = $no;
            $row['name']       = $k->name;
            $row['code']       = $k->code_member;
            $row['created_at'] = tgl_indo($k->date_payment);
            $row['action']     = "<center>
                                        <span class='btn btn-danger btn-flat btn-sm' data-toggle='tooltip' onclick='destroy(".'"'.$k->id.'"'.",".'"'.base_url('_admin/payment/destroy').'"'.", ".'"'.$this->security->get_csrf_hash().'"'.")' title='hapus data'><i class='fa fa-trash-o'></i></span>
                                        <a href='".base_url('_admin/payment/edit/'.$k->id)."'><span class='btn btn-info btn-flat btn-sm' data-toggle='tooltip' title='edit data ".$k->name."'><i class='fa fa-edit'></i></span></a>
                                    </center>";
            $data[]=$row;
        }
        $output = array("data" => $data,);
        echo json_encode($output);
    }
    public function create(){
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('id_member', 'id_member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $data = array(
                    'title'     => 'Admin || tambah data member',
                    'username'  => $this->session->userdata('username'),
                    'token'     => $this->security->get_csrf_token_name(),
                    'value'     => $this->security->get_csrf_hash(),
                    'message' => $this->session->userdata('message'),
                );
            $this->parser->parse("pembayaran/create.tpl",$data);
        }else{
            $this->store();
        }
    }
    public function store()
    {
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('id_member', 'id_member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'name'             => $this->input->post('name'),
                'code_member'      => $this->input->post('id_member'),
                'date_payment'     => date('Y-m-d'),
            );
            $this->member->insert($data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('_admin/payment/create'),'refresh');
        }
    }
    public function destroy()
    {
        $id = $this->input->post('id');
        $this->member->delete($id);
        echo json_encode(array('status' => TRUE, 'token' => $this->security->get_csrf_hash()));
    }
    public function edit()
    {
        $id = $this->uri->segment(4);
        $data = array(
                'title'   => 'Form edit Data',
                'getpayment' => $this->member->find_by_id($id),
                'message' => $this->session->userdata('message'),
                'username' => $this->session->userdata('username'),
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
            );
        $this->parser->parse("pembayaran/edit.tpl",$data);
    }
    public function update(){
        $id = $this->uri->segment(4);
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('id_member', 'id_member', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'name'             => $this->input->post('name'),
                'code_member'      => $this->input->post('id_member'),
                'date_payment'     => date('Y-m-d'),
            );
            $this->member->update($id, $data);
            $this->session->set_flashdata('message', 'Data berhasil diperbarui');
            redirect(base_url('_admin/payment/'),'refresh');
        }
    }
    
}
