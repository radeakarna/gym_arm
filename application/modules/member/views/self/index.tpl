{extends file="index.tpl"}
{block name=header}
<link rel="stylesheet" href="{base_url()}plugins/datetimepicker/css/bootstrap-datepicker.css">
{/block}
{block name=content}
{if isset($getmember->id)}
  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">data diri anda</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          
          <div class="box-body">
            <form method="post" class="form-horizontal" action="{base_url()}member/self_controller/update/{$getmember->id}">
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">No KTP</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold">{$getmember->nisn}</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Tempat Lahir</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold">{$getmember->place_of_birth}</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Tanggal Lahir</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold">{$getmember->date_of_birth}</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Nomor telepon</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold">{$getmember->telp}</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Tinggi badan</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold">{$getmember->hight}</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Berat badan</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold">{$getmember->wight}</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Jenis kelamin</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold">{$getmember->sex}</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Alamat</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold">{$getmember->addres}</h5>
              </div>
            </div>
            
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <!-- <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="{$getmember->nisn}" required/>
                  {form_error('nik')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="{$getmember->place_of_birth}" required/>
                  {form_error('tmp_lahir')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="{$getmember->date_of_birth}" required/>
                  {form_error('tgl_lahir')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" value="{$getmember->telp}" required/>
                  {form_error('no_telepon')}
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tinggi badan</label>
                  <input type="text" class="form-control" name="tinggi" placeholder="Tinggi basan" value="{$getmember->hight}" required/>
                  {form_error('tinggi')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Berat badan</label>
                  <input type="text" class="form-control" name="berat" placeholder="Berat badan" value="{$getmember->wight}" required/>
                  {form_error('berat')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  {form_error('jk')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4">{$getmember->addres}</textarea>
                  {form_error('alamat')}
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="jumbotron" style="padding:10px;">
                <h5>Tinggi badan = {$getmember->hight} cm</h5>
                <h5>Berat badan = {$getmember->wight} kg</h5>
                <h5>Umur saat ini = {hitung_umur($getmember->date_of_birth)} tahun</h5>
                <span><h4>TDEE anda adalah {tdee($getmember->sex,$getmember->wight,$getmember->hight, hitung_umur($getmember->date_of_birth))} Kalori/perhari</h4></span></div>
            </div> -->
            </form>
          </div>
          <div class="box-footer clearfix">
            <!-- <button type="submit" class="btn btn-primary">Simpan data</button> -->
          </div>
          
      </div>

  </section><!-- /.content -->

  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">Latihan yang sudah dilakukan</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          
          <div class="box-body">

            <table class="table table-striped">
              <tr>
                <th style="width: 10px">#</th>
                <th>Jenis latihan</th>
                <th>Tanggal latiha</th>
                <!-- <th style="width: 40px">delete</th> -->
              </tr>
              {assign var="increment" value=1}
              {foreach from=$exercise item=$row}
              <tr>
                <td class="td"  align="center">{$increment}.</td>
                <td class="td" >{$row->exercise}</td>
                <td>
                  {tgl_indo($row->date_exercise)}
                </td>
                <!-- <td><span class="badge bg-red"><i class="fa fa-trash-o"></i></span></td> -->
              </tr>
              {assign var="increment" value=$increment+1}
              {/foreach}
              
            </table>

            <br><br><hr>
            <form method="post" action="{base_url()}member/self_controller/store_excercise">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Latihan dilakukan</label>
                  <textarea name="latihan" class="form-control" rows="4"></textarea>
                  {form_error('latihan')}
                </div>
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Tanggal Latihan</label>
                  <input type="text" class="form-control" id="datepicker_two" name="tanggal_latihan" placeholder="Tanggal pada saat latihan" value="" required/>
                  {form_error('tanggal_latihan')}
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary">Simpan data</button>
          </div>
          </form>
      </div>

  </section><!-- /.content -->
{else}
  {if isset($message)}
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-info">
        <h4>INFO!</h4>
        <p>{$message}</p>
      </div>
    </section>
  {else}
    <section class="content-header">
      <div class="callout callout-danger">
        <h4>Perhatian!</h4>
        <p>Mohon lengkapi data diri terlebih dahulu.</p>
      </div>
    </section>
  {/if}
  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">Melengkapi data diri</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          <form method="post" action="{base_url()}member/self/">
          <div class="box-body">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="" required/>
                  {form_error('nik')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="" required/>
                  {form_error('tmp_lahir')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="" required/>
                  {form_error('tgl_lahir')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" required/>
                  {form_error('no_telepon')}
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tinggi badan</label>
                  <input type="text" class="form-control" name="tinggi" placeholder="Tinggi basan" value="" required/>
                  {form_error('tinggi')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Berat badan</label>
                  <input type="text" class="form-control" name="berat" placeholder="Berat badan" value="" required/>
                  {form_error('berat')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  {form_error('jk')}
                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4"></textarea>
                  {form_error('alamat')}
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary">Simpan data</button>
          </div>
          </form>
      </div>

  </section><!-- /.content -->
{/if}

{/block}
{block name=footer}
<!-- Datetimepicker -->
<script src="{base_url()}plugins/datetimepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    //function get_tanggal(){
        $(function () {
            $('#datepicker').datepicker({
              startView: 2,
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });    

        $(function () {
            $('#datepicker_two').datepicker({
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });    
    //}      
</script>
{/block}