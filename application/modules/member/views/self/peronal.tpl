{extends file="index.tpl"}
{block name=header}
<link rel="stylesheet" href="{base_url()}plugins/datetimepicker/css/bootstrap-datepicker.css">
{/block}
{block name=content}
{if isset($getmember->id)}
  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">data diri anda</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          <form method="post" action="{base_url()}member/member_controller/update/{$getmember->id}">
          <div class="box-body">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="{$getmember->nisn}" required/>
                  {form_error('nik')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="{$getmember->place_of_birth}" required/>
                  {form_error('tmp_lahir')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="{$getmember->date_of_birth}" required/>
                  {form_error('tgl_lahir')}
                </div>
                
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" value="{$getmember->telp}" required/>
                  {form_error('no_telepon')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  {form_error('jk')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4">{$getmember->addres}</textarea>
                  {form_error('alamat')}
                </div>
              </div>
            </div>
            <!-- <div class="form-group">
              <div class="jumbotron" style="padding:10px;">
                <h5>Tinggi badan = {$getmember->hight} cm</h5>
                <h5>Berat badan = {$getmember->wight} kg</h5>
                <h5>Umur saat ini = {hitung_umur($getmember->date_of_birth)} tahun</h5>
                <span><h4>TDEE anda adalah {tdee($getmember->sex,$getmember->wight,$getmember->hight, hitung_umur($getmember->date_of_birth))} Kalori/perhari</h4></span></div>
            </div> -->
          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary">Simpan data</button>
          </div>
          </form>
      </div>

  </section><!-- /.content -->
{else}
  {if isset($message)}
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-info">
        <h4>INFO!</h4>
        <p>{$message}</p>
      </div>
    </section>
  {else}
    <section class="content-header">
      <div class="callout callout-danger">
        <h4>Perhatian!</h4>
        <p>Mohon lengkapi data diri terlebih dahulu.</p>
      </div>
    </section>
  {/if}
  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">Melengkapi data diri</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          <form method="post" action="{base_url()}member/personal">
          <div class="box-body">
            <input type="hidden" name="{$token}" value="{$value}" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="" required/>
                  {form_error('nik')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="" required/>
                  {form_error('tmp_lahir')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="" required/>
                  {form_error('tgl_lahir')}
                </div>
                
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" required/>
                  {form_error('no_telepon')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  {form_error('jk')}
                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4"></textarea>
                  {form_error('alamat')}
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary">Simpan data</button>
          </div>
          </form>
      </div>

  </section><!-- /.content -->
{/if}

{/block}
{block name=footer}
<!-- Datetimepicker -->
<script src="{base_url()}plugins/datetimepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    //function get_tanggal(){
        $(function () {
            $('#datepicker').datepicker({
              startView: 2,
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });    
    //}      
</script>
{/block}