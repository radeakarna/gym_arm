<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model {

  public $table = 'persons_data';
  
  public function __construct()
  {
    parent::__construct();
  }

  public function find()
  {
    $this->db->order_by('id', 'DESC');
    return $this->db->get($this->table)->result();
  }
  
  public function find_by_id($id)
  {
    return $this->db->where('id', $id)->limit(1)->get($this->table)->row();
  }
  public function find_by_id_user($id)
  {
    return $this->db->where('id_user', $id)->limit(1)->get($this->table)->row();
  }
  public function find_user_join_member($id)
  {
    $query = $this->db->where('id', $id)->limit(1)->get('users')->row();
    $member = $this->db->where('id', $query->id_member)->limit(1)->get('member')->row();
    return $member->name;
  }
  public function insert($data)
  {
    $this->db->set($data);
    $this->db->insert($this->table);
    return $this->db->insert_id();
  }
  public function update($id, $data)
  {
    // $this->db->where('id', $id);
    // $this->db->set($data);
    // $this->db->update($this->table);
    // return $this->db->affected_rows();
    return $this->db->update($this->table, $data,  array('id' => $id));
  }
}