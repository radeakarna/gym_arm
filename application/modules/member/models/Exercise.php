<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exercise extends CI_Model {

  public $table = 'exercises';
  
  public function __construct()
  {
    parent::__construct();
  }

  public function find_where_id($id)
  {
    $this->db->order_by('id', 'DESC');
    return $this->db->where('id_user', $id)->get($this->table)->result();
  }

  public function insert($data)
  {
    $this->db->set($data);
    $this->db->insert($this->table);
    return $this->db->insert_id();
  }
}