<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_controller extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('base','user');
        $this->load->model('member','member');
        if($this->session->userdata('status_member') != "loginmember" AND !$this->session->userdata('username_member')){
            redirect(base_url(""));
        }
        
    }

    public function index(){
        $getuser   = $this->user->find_by_id($this->session->userdata('id_login_member'));
        $getmember = $this->member->find_by_id($getuser->id);
        if ($this->member->find_user_join_member($this->session->userdata('id_login_member')) == 'personal') {
            redirect(base_url('member/personal'),'refresh');
        }else{
            redirect(base_url('member/self/'),'refresh');
        }
    }
    public function personal()
    {
        $this->form_validation->set_rules('nik', 'nidn', 'trim|required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('no_telepon', 'Nomor telepon', 'trim|required');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat lahir', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $getuser   = $this->user->find_by_id($this->session->userdata('id_login_member'));
            $getmember = $this->member->find_by_id_user($getuser->id);
            $data = array(
                'title' => 'member || WEB',
                'username'  => $this->session->userdata('username_member'),
                'getuser'   => $getuser,
                'getmember' => $getmember,
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
            $this->parser->parse("self/peronal.tpl",$data);
        }else{
            $this->store();
        }
    }
    public function store()
    {
        $this->form_validation->set_rules('nik', 'nidn', 'trim|required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('no_telepon', 'Nomor telepon', 'trim|required');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat lahir', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'id_user'         => $this->session->userdata('id_login_member'),
                'nisn'            => $this->input->post('nik'),
                'sex'             => $this->input->post('jk'),
                'telp'            => $this->input->post('no_telepon'),
                'addres'          => $this->input->post('alamat'),
                'place_of_birth'  => $this->input->post('tmp_lahir'),
                'date_of_birth'   => $this->input->post('tgl_lahir'),
            );
            $this->member->insert($data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('member/personal'),'refresh');
        }
    }
    public function update()
    {
        $this->form_validation->set_rules('nik', 'nidn', 'trim|required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('no_telepon', 'Nomor telepon', 'trim|required');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat lahir', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect(base_url('member/self/'),'refresh');
        }else{
            $data = array(
                'nisn'            => $this->input->post('nik'),
                'sex'             => $this->input->post('jk'),
                'telp'            => $this->input->post('no_telepon'),
                'addres'          => $this->input->post('alamat'),
                'place_of_birth'  => $this->input->post('tmp_lahir'),
                'date_of_birth'   => $this->input->post('tgl_lahir'),
            );
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            $this->member->update($this->uri->segment(4), $data);
            redirect(base_url('member/personal'),'refresh');
        }
    }
}
