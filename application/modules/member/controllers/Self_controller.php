<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Self_controller extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('base','user');
        $this->load->model('exercise','exercise');
        $this->load->model('member','member');
        if($this->session->userdata('status_member') != "loginmember" AND !$this->session->userdata('username_member')){
            redirect(base_url(""));
        }
        
    }

    public function index(){
        $this->form_validation->set_rules('nik', 'nidn', 'trim|required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('no_telepon', 'Nomor telepon', 'trim|required');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat lahir', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'trim|required');
        $this->form_validation->set_rules('tinggi', 'Tinggi badan', 'trim|required');
        $this->form_validation->set_rules('berat', 'Berat badan', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            $getuser   = $this->user->find_by_id($this->session->userdata('id_login_member'));
            $getmember = $this->member->find_by_id_user($getuser->id);
            $getexercise = $this->exercise->find_where_id($this->session->userdata('id_login_member'));
            $data = array(
                'title' => 'member || WEB',
                'username' => $this->session->userdata('username_member'),
                'getuser' => $getuser,
                'getmember' => $getmember,
                'exercise' => $getexercise,
                'token'     => $this->security->get_csrf_token_name(),
                'value'     => $this->security->get_csrf_hash(),
                'message' => $this->session->userdata('message'),
            );
            $this->parser->parse("self/index.tpl",$data);
        }else{
            $this->store();
        }
    }
    public function store()
    {
        $this->form_validation->set_rules('nik', 'nidn', 'trim|required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('no_telepon', 'Nomor telepon', 'trim|required');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat lahir', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'trim|required');
        $this->form_validation->set_rules('tinggi', 'Tinggi badan', 'trim|required');
        $this->form_validation->set_rules('berat', 'Berat badan', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'id_user'         => $this->session->userdata('id_login_member'),
                'nisn'            => $this->input->post('nik'),
                'sex'             => $this->input->post('jk'),
                'telp'            => $this->input->post('no_telepon'),
                'hight'           => $this->input->post('tinggi'),
                'wight'           => $this->input->post('berat'),
                'addres'          => $this->input->post('alamat'),
                'place_of_birth'  => $this->input->post('tmp_lahir'),
                'date_of_birth'   => $this->input->post('tgl_lahir'),
            );
            $this->member->insert($data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('member/self/'),'refresh');
        }
    }

    public function store_excercise()
    {
        $this->form_validation->set_rules('latihan', 'latian', 'trim|required');
        $this->form_validation->set_rules('tanggal_latihan', 'tanggal latihan', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect_back();
        }else{
            $data = array(
                'id_user'         => $this->session->userdata('id_login_member'),
                'exercise'            => $this->input->post('latihan'),
                'date_exercise'             => $this->input->post('tanggal_latihan'),
            );
            $this->exercise->insert($data);
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            redirect(base_url('member/self/'),'refresh');
        }
    }
    public function update()
    {
        $this->form_validation->set_rules('nik', 'nidn', 'trim|required');
        $this->form_validation->set_rules('jk', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('no_telepon', 'Nomor telepon', 'trim|required');
        $this->form_validation->set_rules('tmp_lahir', 'Tempat lahir', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'trim|required');
        $this->form_validation->set_rules('tinggi', 'Tinggi badan', 'trim|required');
        $this->form_validation->set_rules('berat', 'Berat badan', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        if (!$this->form_validation->run()) {
            redirect(base_url('member/self/'),'refresh');
        }else{
            $data = array(
                'nisn'            => $this->input->post('nik'),
                'sex'             => $this->input->post('jk'),
                'telp'            => $this->input->post('no_telepon'),
                'hight'           => $this->input->post('tinggi'),
                'wight'           => $this->input->post('berat'),
                'addres'          => $this->input->post('alamat'),
                'place_of_birth'  => $this->input->post('tmp_lahir'),
                'date_of_birth'   => $this->input->post('tgl_lahir'),
            );
            $this->session->set_flashdata('message', 'Data berhasil diinputkan');
            $this->member->update($this->uri->segment(4), $data);
            redirect(base_url('member/self/'),'refresh');
        }
    }
    
}
