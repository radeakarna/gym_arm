<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('auth','auth');
    }

    // public function index(){
    //     if($this->auth->is_logged_in() == true){
    //         $data = array(
    //             'token'            => $this->security->get_csrf_token_name(),
    //             'value'            => $this->security->get_csrf_hash(),
    //             );
    //         $this->parser->parse("auth/index.tpl", $data);
    //     }else{
    //         redirect(base_url("member/"));
            
    //     }
    // }
    public function find(){
        $json = array();
        $this->form_validation->set_rules('email','email','required');
        $this->form_validation->set_rules('password','password','required');
        $this->form_validation->set_message('required', 'You missed the input {field}!');
        

        if($this->form_validation->run() != false){
            $username = $this->input->post('email');
            $password = $this->input->post('password');
            $uname = array('username' => $username);
            $cek_user = $this->auth->find($uname)->num_rows();
            if ($cek_user > 0) {
                $upass = array('password' => $password);
                $cek_upass = $this->auth->find($upass)->num_rows();
                if ($cek_upass > 0) {
                    $where = array(
                        'username' => $username,
                        'password' => $password,
                        'role' => 1,
                        );
                    $cek = $this->auth->find($where)->num_rows();
                    $cekdua = $this->auth->find($where);
                    if($cek == 1){
                            $data_session = array(
                            'username_member' =>  $cekdua->row()->username,
                            'id_login_member' =>  $cekdua->row()->id,
                            'status_member' => "loginmember",
                            );
                            $this->session->set_userdata($data_session);
                            $data = $this->session->userdata;
                            redirect(base_url("member/"));
                            // echo json_encode(array('status' => TRUE,'token' => $this->security->get_csrf_hash(), 'result' => base_url().'member'));
                    }else{
                        echo "<script>alert('Anda Gagal Login!');window.history.go(-1);</script>";
                        // $json = array(
                        //     'email' => '',
                        //     'password' => '',
                        //     'status' => '<p class="mt-3 text-danger">Anda gagal login</p>',
                        //     'token' => $this->security->get_csrf_hash()
                        // );
                        // $this->output
                        // ->set_content_type('application/json')
                        // ->set_output(json_encode($json));
                    }
                }else{
                    echo "<script>alert('password salah');window.history.go(-1);</script>";
                    // $json = array(
                    //     'email' => '',
                    //     'password' => '<p class="mt-3 text-danger">Password salah</p>',
                    //     'status' => '',
                    //     'token' => $this->security->get_csrf_hash()
                    // );
                    // $this->output
                    // ->set_content_type('application/json')
                    // ->set_output(json_encode($json));
                }
            }else{
                echo "<script>alert('Username salah');window.history.go(-1);</script>";
                // $json = array(
                //     'email' => '<p class="mt-3 text-danger">Email tidak terdaftar</p>',
                //     'password' => '',
                //     'status' => '',
                //     'token' => $this->security->get_csrf_hash()
                // );
                // $this->output
                // ->set_content_type('application/json')
                // ->set_output(json_encode($json));
            }
        }else{
            redirect(base_url());
            // $json = array(
            //     'email' => form_error('email', '<p class="mt-3 text-danger">', '</p>'),
            //     'password' => form_error('password', '<p class="mt-3 text-danger">', '</p>'),
            //     'token' => $this->security->get_csrf_hash()
            // );
            // $this->output
            // ->set_content_type('application/json')
            // ->set_output(json_encode($json));
        }
        
                  
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect('', 'refresh');
    }
    
}
