<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('user','member');
        $this->member->table='member';
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//$this->load->view('welcome_message');
		$data = array(
			'tittle' => 'Dasboard',
			'captcha' 		   => $this->recaptcha->getWidget(),
	        'script_captcha'   => $this->recaptcha->getScriptTag(),
	        'getCaptcha' 	   => $this->create_captcha(),
	        'token'			   => $this->security->get_csrf_token_name(),
	        'value'			   => $this->security->get_csrf_hash(),
	        'member'		   => $this->member->find_where(array('akses' => 0)),
 	    	);
		$this->parser->parse("layouts/index.tpl",$data);
	}
	public function create_captcha(){
		$vals = array(
                'img_path'=>'./captcha/', #folder captcha yg sudah dibuat tadi
		        'img_url'=>base_url('captcha'), #ini arahnya juga ke folder captcha
		        'img_width'=>'145', #lebar image captcha
		        'img_height'=>'45', #tinggi image captcha
		        'expiration'=>7200, #waktu expired
		        'font_path' => FCPATH . 'assets/font/coolvetica.ttf', #load font jika mau ganti fontnya
		        'pool' => '0123456789', #tipe captcha (angka/huruf, atau kombinasi dari keduanya)
		 
		        # atur warna captcha-nya di sini ya.. gunakan kode RGB
		        'colors' => array(
		                'background' => array(242, 242, 242),
		                'border' => array(255, 255, 255),
		                'text' => array(0, 0, 0),
		                'grid' => array(255, 40, 40))
            );
 
            // create captcha image
            $cap = create_captcha($vals);
 
            // store image html code in a variable
            $image = $cap['image'];
 
            // store the captcha word in a session
            $this->session->set_userdata('mycaptcha', $cap['word']);

            return $image;
	}
	public function check_captcha(){
		$captcha = $this->input->post('captcha'); #mengambil value inputan pengguna
		$word = $this->session->userdata('mycaptcha'); #mengambil value captcha
		if (isset($captcha)) { #cek variabel $captcha kosong/tidak
		   if (strtoupper($captcha)==strtoupper($word)) { #proses pencocokan captcha
		        return TRUE;
		   }
		 }
	}
}
