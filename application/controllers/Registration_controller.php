<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include ('Welcome.php');
class Registration_controller extends Welcome {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('user','user');	
	}

	public function store()
	{
		$json = array();
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
        $this->form_validation->set_rules('jenis', 'Jenis member', 'required');
        $this->form_validation->set_rules('captcha', 'check Captcha', 'trim|callback_check_captcha|required');

        $this->form_validation->set_message('required', 'You missed the input {field}!');

        if (!$this->form_validation->run()) {
            $json = array(
                'name' => form_error('name', '<p class="mt-3 text-danger">', '</p>'),
                'email' => form_error('email', '<p class="mt-3 text-danger">', '</p>'),
                'password' => form_error('password', '<p class="mt-3 text-danger">', '</p>'),
                'confirm_password' => form_error('confirm_password', '<p class="mt-3 text-danger">', '</p>'),
                'jenis' => form_error('jenis', '<p class="mt-3 text-danger">', '</p>'),
                'captcha' => form_error('captcha', '<p class="mt-3 text-danger">', '</p>'),
                'token' => $this->security->get_csrf_hash()
            );
            $this->output
	        ->set_content_type('application/json')
	        ->set_output(json_encode($json));
        }else{
        	$data = array(
				'name'             => $this->input->post('name'),
				'id_member'        => $this->input->post('jenis'),
	            'username'         => $this->input->post('email'),
	            'password'         => md5($this->input->post('password')),
	            'status'           => 'user',
	            'role'		       => 1,
	            'lost_member'      => add_date(date('Y-m-d'),25),
			);
			$this->user->insert($data);
			echo json_encode(array('status' => TRUE,'token' => $this->security->get_csrf_hash(), 'captcha' => $this->recaptcha->getWidget(),'script_captcha' => $this->recaptcha->getScriptTag(), 'getCaptcha' => $this->create_captcha()));
        }
	}
	public function update_csrf()
	{
	  $data['csrf_hash'] = $this->security->get_csrf_hash();
	  echo json_encode($data);
	}
	// public function cek()
	// {
	// 	$this->output
	//         ->set_content_type('application/json')
	//         ->set_output(json_encode(array('status' = TRUE)));
	// }
	// public function validate() {
 //        $json = array();

 //        $this->form_validation->set_rules('name', 'name', 'required');
 //        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
 //        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
 //        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');
 //        $this->form_validation->set_rules('captcha', 'check Captcha', 'trim|callback_check_captcha|required');

 //        $this->form_validation->set_message('required', 'You missed the input {field}!');

 //        if (!$this->form_validation->run()) {
 //            $json = array(
 //                'name' => form_error('name', '<p class="mt-3 text-danger">', '</p>'),
 //                'email' => form_error('email', '<p class="mt-3 text-danger">', '</p>'),
 //                'password' => form_error('password', '<p class="mt-3 text-danger">', '</p>'),
 //                'confirm_password' => form_error('confirm_password', '<p class="mt-3 text-danger">', '</p>'),
 //                'captcha' => form_error('captcha', '<p class="mt-3 text-danger">', '</p>')
 //            );
 //            $this->output
	//         ->set_content_type('application/json')
	//         ->set_output(json_encode($json));
 //        }else{
 //        	// Is the token field set and valid? $this->input->post(get_csrf_token_name()) == get_csrf_hash()
 //    		// $reponse = array(
 //      //           'csrfName' => $this->security->get_csrf_token_name(),
 //      //           'csrfHash' => $this->security->get_csrf_hash()
 //      //           );
 //    		// echo json_encode($reponse);
 //        	$this->store();
 //        }
 //    }
}
