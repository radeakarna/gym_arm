<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('Welcome.php');

class Pages_controller extends welcome {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('trainer');	
	}

	public function contact()
	{
		//$this->load->view('welcome_message');
		$data = array(
			'tittle' => 'Contact us',
			'captcha' 		   => $this->recaptcha->getWidget(),
	        'script_captcha'   => $this->recaptcha->getScriptTag(),
	        'getCaptcha' 	   => $this->create_captcha(),
	        'token'			   => $this->security->get_csrf_token_name(),
	        'value'			   => $this->security->get_csrf_hash(),
	        'member'		   => $this->member->find_where(array('akses' => 0)),
			);
		$this->parser->parse("contact.tpl",$data);
	}

	public function about()
	{
		//$this->load->view('welcome_message');
		$data = array(
			'tittle' => 'About us',
			'captcha' 		   => $this->recaptcha->getWidget(),
	        'script_captcha'   => $this->recaptcha->getScriptTag(),
	        'getCaptcha' 	   => $this->create_captcha(),
	        'token'			   => $this->security->get_csrf_token_name(),
	        'value'			   => $this->security->get_csrf_hash(),
	        'member'		   => $this->member->find_where(array('akses' => 0)),
			);
		$this->parser->parse("about.tpl",$data);
	}

	public function trainer()
	{
		//$this->load->view('welcome_message');
		$data = array(
			'tittle' => 'Trainer',
	        'getCaptcha' 	   => $this->create_captcha(),
	        'token'			   => $this->security->get_csrf_token_name(),
	        'value'			   => $this->security->get_csrf_hash(),
	        'member'		   => $this->trainer->get(),
			);
		$this->parser->parse("trainer.tpl",$data);
	}

	public function signup()
	{
		//$this->load->view('welcome_message');
		$data = array(
			'tittle' => 'About us',
			'captcha' 		   => $this->recaptcha->getWidget(),
	        'script_captcha'   => $this->recaptcha->getScriptTag(),
	        'getCaptcha' 	   => $this->create_captcha(),
	        'token'			   => $this->security->get_csrf_token_name(),
	        'value'			   => $this->security->get_csrf_hash(),
	        'member'		   => $this->member->find_where(array('akses' => 0)),
			);
		$this->parser->parse("signup.tpl",$data);
	}
}
