<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'ewelcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
//$route['_admin'] = '_admin/aut_login';
$route['_admin'] = '_admin/admin_controller';
$route['_admin/auth'] = '_admin/auth_controller';
$route['_admin/auth/auth_login'] = '_admin/auth_controller/find';

//admin member
$route['_admin/member'] = '_admin/member_controller';
$route['_admin/member/create'] = '_admin/member_controller/create';
$route['_admin/member/edit/(:any)'] = '_admin/member_controller/edit/$i';
$route['_admin/member/update/(:any)'] = '_admin/member_controller/update/$1';
$route['_admin/member/show'] = '_admin/member_controller/show';
$route['_admin/member/store'] = '_admin/member_controller/store';
$route['_admin/member/destroy'] = '_admin/member_controller/destroy';

//admin member individu
$route['_admin/individu'] = '_admin/individu_controller';
$route['_admin/individu/show'] = '_admin/individu_controller/show';
$route['_admin/individu/create'] = '_admin/individu_controller/create';
$route['_admin/individu/edit/(:any)'] = '_admin/individu_controller/edit/$i';
$route['_admin/individu/update/(:any)'] = '_admin/individu_controller/update/$1';
$route['_admin/individu/store'] = '_admin/individu_controller/store';
$route['_admin/individu/destroy'] = '_admin/individu_controller/destroy';

//admin member personal
$route['_admin/personal'] = '_admin/personal_controller';
$route['_admin/personal/show'] = '_admin/personal_controller/show';
$route['_admin/personal/create'] = '_admin/personal_controller/create';
$route['_admin/personal/edit/(:any)'] = '_admin/personal_controller/edit/$i';
$route['_admin/personal/update/(:any)'] = '_admin/personal_controller/update/$1';
$route['_admin/personal/store'] = '_admin/personal_controller/store';
$route['_admin/personal/destroy'] = '_admin/personal_controller/destroy';
$route['_admin/personal/continue'] = '_admin/personal_controller/continue_member';

//admin member personal trainer
$route['_admin/personal_trainer'] = '_admin/personal_trainer_controller';
$route['_admin/personal_trainer/show'] = '_admin/personal_trainer_controller/show';
$route['_admin/personal_trainer/create'] = '_admin/personal_trainer_controller/create';
$route['_admin/personal_trainer/edit/(:any)'] = '_admin/personal_trainer_controller/edit/$i';
$route['_admin/personal_trainer/update/(:any)'] = '_admin/personal_trainer_controller/update/$1';
$route['_admin/personal_trainer/store'] = '_admin/personal_trainer_controller/store';
$route['_admin/personal_trainer/destroy'] = '_admin/personal_trainer_controller/destroy';
$route['_admin/personal_trainer/continue'] = '_admin/personal_trainer_controller/continue_member';

//admin payment personal
$route['_admin/payment_individu'] = '_admin/payments_individu_controller';
$route['_admin/payment_individu/show'] = '_admin/payments_individu_controller/show';
$route['_admin/payment_individu/create'] = '_admin/payments_individu_controller/create';
$route['_admin/payment_individu/find_user'] = '_admin/payments_individu_controller/find_user';
$route['_admin/payment_individu/store'] = '_admin/payments_individu_controller/store';
$route['_admin/payment_individu/destroy'] = '_admin/payments_individu_controller/destroy';

//admin payment personal
$route['_admin/payment_personal'] = '_admin/payments_personal_controller';
$route['_admin/payment_personal/show'] = '_admin/payments_personal_controller/show';
$route['_admin/payment_personal/create'] = '_admin/payments_personal_controller/create';
$route['_admin/payment_personal/destroy'] = '_admin/payments_personal_controller/destroy';

//admin payment personal trainer
$route['_admin/payment_personal_trainer'] = '_admin/payments_personal_trainer_controller';
$route['_admin/payment_personal_trainer/show'] = '_admin/payments_personal_trainer_controller/show';
$route['_admin/payment_personal_trainer/create'] = '_admin/payments_personal_trainer_controller/create';
$route['_admin/payment_personal_trainer/destroy'] = '_admin/payments_personal_trainer_controller/destroy';


//admin tools
$route['_admin/tool'] = '_admin/tools_controller';
$route['_admin/tool/create'] = '_admin/tools_controller/create';
$route['_admin/tool/show'] = '_admin/tools_controller/show';
$route['_admin/tool/store'] = '_admin/tools_controller/store';

//admin payments
$route['_admin/payment'] = '_admin/payments_controller';
$route['_admin/payment/create'] = '_admin/payments_controller/create';
$route['_admin/payment/show'] = '_admin/payments_controller/show';
// $route['_admin/tool/store'] = '_admin/tools_controller/store';
$route['_admin/payment/edit/(:any)'] = '_admin/payments_controller/edit/$1';
$route['_admin/payment/update/(:any)'] = '_admin/payments_controller/update/$1';
$route['_admin/payment/destroy'] = '_admin/payments_controller/destroy';

//admin Settings
$route['_admin/setting'] = '_admin/settings_controller';
$route['_admin/setting/about'] = '_admin/settings_controller/about';
$route['_admin/setting/cost'] = '_admin/settings_controller/cost';
$route['_admin/setting/update_facebook'] = '_admin/settings_controller/update_facebook';
$route['_admin/setting/update_instagram'] = '_admin/settings_controller/update_instagram';
$route['_admin/setting/update_twitter'] = '_admin/settings_controller/update_twitter';
$route['_admin/setting/update_gmail'] = '_admin/settings_controller/update_gmail';
$route['_admin/setting/update_contact'] = '_admin/settings_controller/update_contact';
$route['_admin/setting/update_addres'] = '_admin/settings_controller/update_addres';
$route['_admin/setting/update_about'] = '_admin/settings_controller/update_about';


//admin presences 
//admin personal presences
$route['_admin/personal_presences'] = '_admin/personal_presences_controller';
$route['_admin/personal_presences/presence/(:num)'] = '_admin/personal_presences_controller/presence/$1';
$route['_admin/personal_presences/show'] = '_admin/personal_presences_controller/show';
$route['_admin/personal_presences/show_member'] = '_admin/personal_presences_controller/show_member';
$route['_admin/personal_presences/create'] = '_admin/personal_presences_controller/create';
$route['_admin/personal_presences/store'] = '_admin/personal_presences_controller/store';
$route['_admin/personal_presences/destroy'] = '_admin/personal_presences_controller/destroy';
//admin personal trainer presences
$route['_admin/personal_trainer_presences'] = '_admin/personal_trainer_presences_controller';
$route['_admin/personal_trainer_presences/precence/(:num)'] = '_admin/personal_trainer_presences_controller/precence/$1';
$route['_admin/personal_trainer_presences/show'] = '_admin/personal_trainer_presences_controller/show';
$route['_admin/personal_trainer_presences/show_member'] = '_admin/personal_trainer_presences_controller/show_member';
$route['_admin/personal_trainer_presences/create'] = '_admin/personal_trainer_presences_controller/create';
$route['_admin/personal_trainer_presences/store'] = '_admin/personal_trainer_presences_controller/store';
$route['_admin/personal_trainer_presences/destroy'] = '_admin/personal_trainer_presences_controller/destroy';


//admin trainer
$route['_admin/trainer'] = '_admin/trainer_controller/index';
$route['_admin/trainer/show'] = '_admin/trainer_controller/show';
$route['_admin/trainer/create'] = '_admin/trainer_controller/create';
$route['_admin/trainer/store'] = '_admin/trainer_controller/store';
$route['_admin/trainer/destroy'] = '_admin/trainer_controller/destroy';
$route['_admin/trainer/edit/(:any)'] = '_admin/trainer_controller/edit/$i';
$route['_admin/trainer/update/(:any)'] = '_admin/trainer_controller/update/$1';


//user 
//user pages
$route['pages/contact'] = 'pages_controller/contact';
$route['pages/about'] = 'pages_controller/about';
$route['pages/trainer'] = 'pages_controller/trainer';
$route['pages/signup'] = 'pages_controller/signup';
//user registration
$route['registration/store'] = 'registration_controller/store';


//member
$route['member'] = 'member/member_controller';
$route['member/personal'] = 'member/member_controller/personal';

//member self
$route['member/self'] = 'member/self_controller';
$route['member/self/store'] = 'member/self_controller/store';
$route['member/self/(:num)']['UPDATE'] = 'member/self/update/$1';