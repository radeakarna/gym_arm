$(function() {

    var cart = Morris.Bar({
        element: 'morris-bar-chart',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Jumlah pemilih'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"page/view_polling.php",
        success:function(data) {
           cart.setData(data);
        }
    });

    
});
