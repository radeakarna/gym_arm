function reload_table(){
    menu_oTables.ajax.reload(null,false); //reload datatable ajax 
}
function destroy(id_emu, address, token){
 // ajax delete data to database
  swal({
      title: 'Are you sure?',
      text: "You won't be able to delete this!",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: true
    },
    function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: "POST",
          data: {id:id_emu,csrf_test_name:token},
          dataType:"json",
          url: address,
            success: function(data) {
                $('#csrf_test_name').val(data.token);
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              console.log(errorThrown);
              $('#csrf_test_name').val(data.token);
              swal("Error deleting!", "Please try again", "error");
            }
        });
      }
        
    });
}

function continou(id_emu, address, token){
 // ajax delete data to database
  swal({
      title: 'Member',
      text: "Perpanjang member ini?",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: "Yes, perpanjang!",
      closeOnConfirm: true
    },
    function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type: "POST",
          data: {id:id_emu,csrf_test_name:token},
          dataType:"json",
          url: address,
            success: function(data) {
                $('#csrf_test_name').val(data.token);
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              console.log(errorThrown);
              $('#csrf_test_name').val(data.token);
              swal("Error deleting!", "Please try again", "error");
            }
        });
      }
        
    });
}