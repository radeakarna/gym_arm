<?php
/* Smarty version 3.1.33, created on 2019-09-15 11:26:39
  from 'C:\laragon\www\gym\application\modules\_admin\views\settings\medsos.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7e1fef21f4a7_97913594',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4b8d0c11e0986647577d6d4b6b7c5dcbbc780cb2' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\settings\\medsos.tpl',
      1 => 1568546759,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7e1fef21f4a7_97913594 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10539030125d7e1fef205081_79528653', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18405183885d7e1fef21c9c0_26149296', 'footer');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'content'} */
class Block_10539030125d7e1fef205081_79528653 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_10539030125d7e1fef205081_79528653',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <style>
      .kv-avatar .krajee-default.file-preview-frame,.kv-avatar .krajee-default.file-preview-frame:hover {
          margin: 0;
          padding: 0;
          border: none;
          box-shadow: none;
          text-align: center;
      }
      .kv-avatar .file-input {
          display: table-cell;
          max-width: 250px;
      }
      .kv-reqd {
          color: red;
          font-family: monospace;
          font-weight: normal;
      }
  </style>
  <?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-success">
        <h4>INFO!</h4>
        <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
      </div>
    </section>
  <?php }?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-md-6">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Sosial Meida</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-10">
                    <p class="margin">Facebok</code></p>
                    <form method="POST" action="<?php echo base_url();?>
_admin/setting/update_facebook/">
                    <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="url" name="facebook" value="<?php echo get_metsos('facebook');?>
" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                    <p class="margin">Instragram</p>
                    <form method="POST" action="<?php echo base_url();?>
_admin/setting/update_instagram">
                    <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="url" name="instragram" value="<?php echo get_metsos('instagram');?>
" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                    <p class="margin">Twitter</p>
                    <form method="POST" action="<?php echo base_url();?>
_admin/setting/update_twitter">
                    <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="url" name="twitter" value="<?php echo get_metsos('twitter');?>
" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->

                    </form>

                  </div><!-- /.col -->
                </div><!-- /.row -->
              </div><!-- ./box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->

          <div class="col-md-6">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Contact person</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-10">
                    <p class="margin">Nomor hp</code></p>
                    <form method="POST" action="<?php echo base_url();?>
_admin/setting/update_contact">
                    <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="text" name="contact" value="<?php echo get_metsos('contact');?>
" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                    <p class="margin">Gmail</p>
                    <form method="POST" action="<?php echo base_url();?>
_admin/setting/update_gmail">
                    <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="email" name="gmail" value="<?php echo get_metsos('gmail');?>
" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                    <p class="margin">Alamat</p>
                    <form method="POST" action="<?php echo base_url();?>
_admin/setting/update_addres">
                    <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                    <div class="input-group input-group-sm">
                      <input type="text" name="addres" value="<?php echo get_metsos('addres_gym');?>
" class="form-control" required>
                      <span class="input-group-btn">
                        <button class="btn btn-info btn-flat" type="submit">Ubah!</button>
                      </span>
                    </div><!-- /input-group -->
                    </form>

                  </div><!-- /.col -->
                </div><!-- /.row -->
              </div><!-- ./box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->
        </diV>
    </section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_18405183885d7e1fef21c9c0_26149296 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_18405183885d7e1fef21c9c0_26149296',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <!-- validator -->
    <?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/validator.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript">
      var menu_oTables="";
      $(document).ready(function(){
        menu_oTables = $('#extmdm');
      });
      function reload_table(){
          menu_oTables.ajax.reload(null,false); //reload datatable ajax 
      }
      function hpsdt(id_emu){
       // ajax delete data to database
        swal({
            title: 'Are you sure?',
            text: "You won't be able to delete this!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              $.ajax({
                type: "POST",
                data: {
                  id:id_emu
                },
                dataType:"HTML",
                url: "hapusdata",
                  success: function(data) {
                      reload_table();
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    swal("Error deleting!", "Please try again", "error");
                  }
              });
            }
              
          });
      }
    <?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'footer'} */
}
