<?php
/* Smarty version 3.1.33, created on 2019-09-18 03:59:01
  from 'C:\laragon\www\gym\application\modules\_admin\views\member\personal\edit.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d81ab850a2659_26646418',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0d89f3f6e20b39b54fac612147aa31ca2ab3e5fd' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\member\\personal\\edit.tpl',
      1 => 1568779135,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d81ab850a2659_26646418 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5382565765d81ab85082bd8_45965198', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9790052225d81ab85089993_66258487', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15367992185d81ab850a0d19_39515690', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_5382565765d81ab85082bd8_45965198 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_5382565765d81ab85082bd8_45965198',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<link rel="stylesheet" href="<?php echo base_url();?>
plugins/datetimepicker/css/bootstrap-datepicker.css">
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_9790052225d81ab85089993_66258487 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_9790052225d81ab85089993_66258487',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="<?php echo base_url();?>
_admin/personal/update/<?php echo $_smarty_tpl->tpl_vars['get_member']->value->id_user;?>
">
        <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->name;?>
" required/>
                <?php echo form_error('name');?>

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">No KTP</label>
                <input type="text" class="form-control" name="nik" placeholder="No KTP" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->nisn;?>
" required/>
                <?php echo form_error('nik');?>

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Tempat Lahir</label>
                <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->place_of_birth;?>
" required/>
                <?php echo form_error('tmp_lahir');?>

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Tanggal Lahir</label>
                <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->date_of_birth;?>
" required/>
                <?php echo form_error('tgl_lahir');?>

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Nomor telepon</label>
                <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->telp;?>
" required/>
                <?php echo form_error('no_telepon');?>

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Jenis kelamin</label>
                <select name="jk" class="form-control">
                  <option value="laki-laki">laki-laki</option>
                  <option value="perempuan">perempuan</option>
                </select>
                <?php echo form_error('jk');?>

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4"><?php echo $_smarty_tpl->tpl_vars['get_member']->value->addres;?>
 </textarea>
                  <?php echo form_error('alamat');?>

            </div>
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_15367992185d81ab850a0d19_39515690 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_15367992185d81ab850a0d19_39515690',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Datetimepicker -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/datetimepicker/js/bootstrap-datepicker.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    //function get_tanggal(){
        $(function () {
            $('#datepicker').datepicker({
              startView: 2,
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });   
    //}      
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'footer'} */
}
