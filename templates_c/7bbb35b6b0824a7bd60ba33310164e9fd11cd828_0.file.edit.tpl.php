<?php
/* Smarty version 3.1.33, created on 2019-10-29 15:03:58
  from 'C:\laragon\www\gym\application\modules\_admin\views\trainer\edit.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db854dee35ba1_81322901',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7bbb35b6b0824a7bd60ba33310164e9fd11cd828' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\trainer\\edit.tpl',
      1 => 1572361437,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db854dee35ba1_81322901 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_511332085db854dee1f5b4_69856431', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6255144335db854dee24eb0_46590301', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_511332085db854dee1f5b4_69856431 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_511332085db854dee1f5b4_69856431',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<link rel="stylesheet" href="<?php echo base_url();?>
plugins/datetimepicker/css/bootstrap-datepicker.css">
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_6255144335db854dee24eb0_46590301 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_6255144335db854dee24eb0_46590301',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Edit Trainer</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="<?php echo base_url();?>
_admin/trainer/update/<?php echo $_smarty_tpl->tpl_vars['get_trainer']->value->id;?>
" enctype='multipart/form-data'>
        <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="<?php echo $_smarty_tpl->tpl_vars['get_trainer']->value->nama;?>
" required/>
              <?php echo form_error('name');?>

            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Jenis kelamin</label>
              <select name="jk" class="form-control">
                <option value="laki-laki" <?php if ($_smarty_tpl->tpl_vars['get_trainer']->value->jk == 'laki-laki') {?> selected <?php }?>>laki-laki</option>
                <option value="perempuan" <?php if ($_smarty_tpl->tpl_vars['get_trainer']->value->jk == 'perempuan') {?> selected <?php }?>>perempuan</option>
              </select>
              <?php echo form_error('jk');?>

            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Alamat</label>
              <textarea name="alamat" class="form-control" rows="4"><?php echo $_smarty_tpl->tpl_vars['get_trainer']->value->alamat;?>
</textarea>
              <?php echo form_error('alamat');?>

            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Foto</label>
              <input type="file" name="foto" required/>
              <?php echo form_error('foto');?>

            </div>
          
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
}
