<?php
/* Smarty version 3.1.33, created on 2019-09-13 04:15:32
  from 'C:\laragon\www\gym\application\modules\member\views\self\peronal.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7b17e4b14683_04210915',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '33249f187644254af52a6e95cc079a207adac60f' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\member\\views\\self\\peronal.tpl',
      1 => 1568348086,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7b17e4b14683_04210915 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10678457885d7b17e4aea290_53708004', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5529592645d7b17e4af0cb8_60890187', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10220393485d7b17e4b129e1_24759921', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_10678457885d7b17e4aea290_53708004 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_10678457885d7b17e4aea290_53708004',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<link rel="stylesheet" href="<?php echo base_url();?>
plugins/datetimepicker/css/bootstrap-datepicker.css">
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_5529592645d7b17e4af0cb8_60890187 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_5529592645d7b17e4af0cb8_60890187',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['getmember']->value->id)) {?>
  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">data diri anda</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          <form method="post" action="<?php echo base_url();?>
member/member_controller/update/<?php echo $_smarty_tpl->tpl_vars['getmember']->value->id;?>
">
          <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->nisn;?>
" required/>
                  <?php echo form_error('nik');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->place_of_birth;?>
" required/>
                  <?php echo form_error('tmp_lahir');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->date_of_birth;?>
" required/>
                  <?php echo form_error('tgl_lahir');?>

                </div>
                
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->telp;?>
" required/>
                  <?php echo form_error('no_telepon');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  <?php echo form_error('jk');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->addres;?>
</textarea>
                  <?php echo form_error('alamat');?>

                </div>
              </div>
            </div>
            <!-- <div class="form-group">
              <div class="jumbotron" style="padding:10px;">
                <h5>Tinggi badan = <?php echo $_smarty_tpl->tpl_vars['getmember']->value->hight;?>
 cm</h5>
                <h5>Berat badan = <?php echo $_smarty_tpl->tpl_vars['getmember']->value->wight;?>
 kg</h5>
                <h5>Umur saat ini = <?php echo hitung_umur($_smarty_tpl->tpl_vars['getmember']->value->date_of_birth);?>
 tahun</h5>
                <span><h4>TDEE anda adalah <?php echo tdee($_smarty_tpl->tpl_vars['getmember']->value->sex,$_smarty_tpl->tpl_vars['getmember']->value->wight,$_smarty_tpl->tpl_vars['getmember']->value->hight,hitung_umur($_smarty_tpl->tpl_vars['getmember']->value->date_of_birth));?>
 Kalori/perhari</h4></span></div>
            </div> -->
          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary">Simpan data</button>
          </div>
          </form>
      </div>

  </section><!-- /.content -->
<?php } else { ?>
  <?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-info">
        <h4>INFO!</h4>
        <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
      </div>
    </section>
  <?php } else { ?>
    <section class="content-header">
      <div class="callout callout-danger">
        <h4>Perhatian!</h4>
        <p>Mohon lengkapi data diri terlebih dahulu.</p>
      </div>
    </section>
  <?php }?>
  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">Melengkapi data diri</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          <form method="post" action="<?php echo base_url();?>
member/personal">
          <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="" required/>
                  <?php echo form_error('nik');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="" required/>
                  <?php echo form_error('tmp_lahir');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="" required/>
                  <?php echo form_error('tgl_lahir');?>

                </div>
                
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" required/>
                  <?php echo form_error('no_telepon');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  <?php echo form_error('jk');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4"></textarea>
                  <?php echo form_error('alamat');?>

                </div>
              </div>
            </div>
          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary">Simpan data</button>
          </div>
          </form>
      </div>

  </section><!-- /.content -->
<?php }?>

<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_10220393485d7b17e4b129e1_24759921 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_10220393485d7b17e4b129e1_24759921',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Datetimepicker -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/datetimepicker/js/bootstrap-datepicker.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    //function get_tanggal(){
        $(function () {
            $('#datepicker').datepicker({
              startView: 2,
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });    
    //}      
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'footer'} */
}
