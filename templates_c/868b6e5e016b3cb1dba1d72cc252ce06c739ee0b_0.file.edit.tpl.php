<?php
/* Smarty version 3.1.33, created on 2019-09-14 00:19:01
  from 'C:\laragon\www\gym\application\modules\_admin\views\pembayaran\edit.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7c31f55119d8_04266100',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '868b6e5e016b3cb1dba1d72cc252ce06c739ee0b' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\pembayaran\\edit.tpl',
      1 => 1568420242,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7c31f55119d8_04266100 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4937774705d7c31f54f35f1_72588025', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'content'} */
class Block_4937774705d7c31f54f35f1_72588025 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_4937774705d7c31f54f35f1_72588025',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="<?php echo base_url();?>
_admin/payment/update/<?php echo $_smarty_tpl->tpl_vars['getpayment']->value->id;?>
">
        <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="<?php echo $_smarty_tpl->tpl_vars['getpayment']->value->name;?>
" required/>
                <?php echo form_error('name');?>

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">ID member</label>
                <input type="text" class="form-control" name="id_member" placeholder="Kode member" value="<?php echo $_smarty_tpl->tpl_vars['getpayment']->value->code_member;?>
" required/>
                <?php echo form_error('name');?>

            </div>
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
}
