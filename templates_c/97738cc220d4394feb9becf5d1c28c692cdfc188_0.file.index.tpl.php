<?php
/* Smarty version 3.1.33, created on 2019-09-26 08:03:23
  from 'C:\laragon\www\gym\application\modules\_admin\views\pembayaran\individu\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d8c70cb514f96_53494162',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '97738cc220d4394feb9becf5d1c28c692cdfc188' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\pembayaran\\individu\\index.tpl',
      1 => 1569484999,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d8c70cb514f96_53494162 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3544931095d8c70cb46a509_82457144', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19374031125d8c70cb4751f0_87359464', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6853543625d8c70cb510f96_77041638', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_3544931095d8c70cb46a509_82457144 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_3544931095d8c70cb46a509_82457144',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- data tables-->
    <link href="<?php echo base_url();?>
plugins/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_19374031125d8c70cb4751f0_87359464 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_19374031125d8c70cb4751f0_87359464',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>

<section class="content">
    <!-- <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Pembayaran</h3>
        
      </div>
      <div class="box-body">
        <form method="POST" action="<?php echo base_url();?>
_admin/payment_individu/create">
          <input type="hidden" id='<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
' name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
          <div class="input-group">
            <input type="text" class="form-control" name="id_member" id="id_member" placeholder="Kode member" value="" required/>
            <span class="input-group-btn">
              <button type="submit" class="btn btn-success btn-flat">Bayar</button>
            </span>
          </div>
        </form>
      </div>
    </div> -->
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Pembayaran</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                <a href="<?php echo base_url();?>
_admin/payment_individu/create"><button type="button" class="btn btn-info btn-flat btn-sm"><i class="fa fa-plus"></i> Tambah data</button></a>
            </div><!-- /. tools -->
        </div>
        <div class="box-body">
          <input type="hidden" id='<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
' name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
          <div class="table-responsive">
            <table id="data-payment" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Jumlah bayar</th>
                        <th>Tangal pembayaran</th>
                        <th><center>action</center></th>
                    </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>  
          </div>
        </div>
        <div class="box-footer clearfix">
          
        </div>
    </div>

</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_6853543625d8c70cb510f96_77041638 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_6853543625d8c70cb510f96_77041638',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

 <!-- data tables -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/media/js/jquery.dataTables.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/media/js/dataTables.bootstrap.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/admin/js/member.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
var menu_oTables="";
$(document).ready(function(){
  menu_oTables = $('#data-payment').DataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": true,

       "ajax": {
          "url": '<?php echo base_url();?>
_admin/payment_individu/show',
          "type": "POST",
          "dataType" : "json",
          "data": function ( d ) {
              d.csrf_test_name = $('#csrf_test_name').val();
          },
       },
       "columns": [
          { 
            "data": "no" 
          },
          { 
            "data": "name" 
          },
          { 
            "data": "cost" 
          },
          { 
            "data": "created_at" 
          },
          {
           "data": "action" 
         },
          ]
  });
});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'footer'} */
}
