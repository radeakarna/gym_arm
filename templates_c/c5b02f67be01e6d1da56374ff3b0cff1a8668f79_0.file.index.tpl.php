<?php
/* Smarty version 3.1.33, created on 2019-10-29 15:06:55
  from 'C:\laragon\www\gym\application\modules\_admin\views\trainer\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db8558f5513c5_10297321',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c5b02f67be01e6d1da56374ff3b0cff1a8668f79' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\trainer\\index.tpl',
      1 => 1572361613,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db8558f5513c5_10297321 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21141934755db8558f53d629_85195202', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15223303175db8558f542ec3_12447638', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14553182565db8558f54d9c7_86058147', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_21141934755db8558f53d629_85195202 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_21141934755db8558f53d629_85195202',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- data tables-->
    <link href="<?php echo base_url();?>
plugins/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_15223303175db8558f542ec3_12447638 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_15223303175db8558f542ec3_12447638',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>

<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Trainer</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                <a href="<?php echo base_url();?>
_admin/trainer/create"><button type="button" class="btn btn-info btn-flat btn-sm"><i class="fa fa-plus"></i> Tambah data</button></a>
            </div><!-- /. tools -->
        </div>
        <div class="box-body">
          <input type="hidden" id='<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
' name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
          <div class="table-responsive">
            <table id="data-payment" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Foto</th>
                        <th>Alamat</th>
                        <th>Jenis kalamin</th>
                        <th><center>action</center></th>
                    </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>  
          </div>
        </div>
        <div class="box-footer clearfix">
          
        </div>
    </div>

</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_14553182565db8558f54d9c7_86058147 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_14553182565db8558f54d9c7_86058147',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

 <!-- data tables -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/media/js/jquery.dataTables.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/media/js/dataTables.bootstrap.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/admin/js/member.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
var menu_oTables="";
$(document).ready(function(){
  menu_oTables = $('#data-payment').DataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": true,

       "ajax": {
          "url": '<?php echo base_url();?>
_admin/trainer/show',
          "type": "GET",
          "dataType" : "json",
       },
       "columns": [
          { 
            "data": "no" 
          },
          { 
            "data": "nama" 
          },
          { 
            "data": "foto" 
          },
          { 
            "data": "alamat" 
          },
          { 
            "data": "jk" 
          },
          { 
            "data": "action" 
          }
          ]
  });
});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'footer'} */
}
