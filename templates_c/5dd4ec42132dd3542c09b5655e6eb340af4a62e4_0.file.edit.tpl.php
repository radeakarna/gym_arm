<?php
/* Smarty version 3.1.33, created on 2019-09-18 12:49:48
  from 'C:\laragon\www\gym\application\modules\_admin\views\member\personal_trainer\edit.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d8227ec1f10b2_43080055',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5dd4ec42132dd3542c09b5655e6eb340af4a62e4' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\member\\personal_trainer\\edit.tpl',
      1 => 1568810980,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d8227ec1f10b2_43080055 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7036299965d8227ec1b9cb9_46843868', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5431451025d8227ec1bf5d6_47686876', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15954133515d8227ec1ef759_74890372', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_7036299965d8227ec1b9cb9_46843868 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_7036299965d8227ec1b9cb9_46843868',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<link rel="stylesheet" href="<?php echo base_url();?>
plugins/datetimepicker/css/bootstrap-datepicker.css">
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_5431451025d8227ec1bf5d6_47686876 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_5431451025d8227ec1bf5d6_47686876',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="<?php echo base_url();?>
_admin/personal_trainer/update/<?php echo $_smarty_tpl->tpl_vars['get_member']->value->id_user;?>
">
        <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->name;?>
" required/>
                  <?php echo form_error('name');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">email</label>
                  <input type="email" class="form-control" name="email" placeholder="Email aktif" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->username;?>
" readonly required/>
                  <?php echo form_error('email');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Jenis member</label>
                  <select class="form-control" name="jenis" required/>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['get_member_tipe']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                      <?php ob_start();
echo $_smarty_tpl->tpl_vars['row']->value->id;
$_prefixVariable1 = ob_get_clean();
if ($_prefixVariable1 == $_smarty_tpl->tpl_vars['get_member']->value->id_member) {?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['row']->value->id;?>
" selected><?php echo $_smarty_tpl->tpl_vars['row']->value->name;?>
</option>
                      <?php } else { ?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['row']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value->name;?>
</option>
                      <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  </select>
                  <?php echo form_error('jenis');?>

                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="password" class="form-control" name="password" value="" >
                  <?php echo form_error('password');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Confirm Password</label>
                  <input type="password" class="form-control" name="confirm_password" value="" >
                  <?php echo form_error('confirm_password');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Aktive member</label>
                  <input type="text" class="form-control" id="datepicker" name="akun" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->lost_member;?>
" >
                  <?php echo form_error('akun');?>

                </div>
              </div>
            </div>
            
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->nisn;?>
" required/>
                  <?php echo form_error('nik');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->place_of_birth;?>
" required/>
                  <?php echo form_error('tmp_lahir');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->date_of_birth;?>
" required/>
                  <?php echo form_error('tgl_lahir');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->telp;?>
" required/>
                  <?php echo form_error('no_telepon');?>

                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tinggi badan</label>
                  <input type="text" class="form-control" name="tinggi" placeholder="Tinggi basan" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->hight;?>
" required/>
                  <?php echo form_error('tinggi');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Berat badan</label>
                  <input type="text" class="form-control" name="berat" placeholder="Berat badan" value="<?php echo $_smarty_tpl->tpl_vars['get_member']->value->wight;?>
" required/>
                  <?php echo form_error('berat');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  <?php echo form_error('jk');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4"><?php echo $_smarty_tpl->tpl_vars['get_member']->value->addres;?>
</textarea>
                  <?php echo form_error('alamat');?>

                </div>
              </div>
            </div>
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_15954133515d8227ec1ef759_74890372 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_15954133515d8227ec1ef759_74890372',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Datetimepicker -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/datetimepicker/js/bootstrap-datepicker.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    //function get_tanggal(){
        $(function () {
            $('#datepicker').datepicker({
              startView: 2,
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });   
    //}      
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'footer'} */
}
