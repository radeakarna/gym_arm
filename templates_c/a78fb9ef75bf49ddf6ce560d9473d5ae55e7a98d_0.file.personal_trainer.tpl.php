<?php
/* Smarty version 3.1.33, created on 2019-10-06 09:22:16
  from 'C:\laragon\www\gym\application\modules\_admin\views\chart\personal_trainer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d99b248333123_77855962',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a78fb9ef75bf49ddf6ce560d9473d5ae55e7a98d' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\chart\\personal_trainer.tpl',
      1 => 1570352464,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d99b248333123_77855962 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18019410455d99b2483277b5_06726648', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7851790895d99b24832d3a2_69830672', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18261928205d99b24832e704_40348338', 'footer');
?>
_trainer<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_18019410455d99b2483277b5_06726648 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_18019410455d99b2483277b5_06726648',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Morris Charts CSS -->
<link href="<?php echo base_url();?>
plugins/moris_chart/morris.css" rel="stylesheet">
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_7851790895d99b24832d3a2_69830672 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_7851790895d99b24832d3a2_69830672',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<section class="content">
    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan harian</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan bulanan</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart-bulan" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan Pertahun</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart-tahun" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_18261928205d99b24832e704_40348338 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_18261928205d99b24832e704_40348338',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Morris Charts JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/moris_chart/raphael.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/moris_chart/morris.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"<?php echo base_url();?>
_admin/chart_controller/get_personal_trainer_data_day",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});

$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart-bulan',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"<?php echo base_url();?>
_admin/chart_controller/get_personal_trainer_data_month",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});

$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart-tahun',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"<?php echo base_url();?>
_admin/chart_controller/get_personal_trainer_data_year",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});
<?php echo '</script'; ?>
>

<?php
}
}
/* {/block 'footer'} */
}
