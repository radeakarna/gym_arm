<?php
/* Smarty version 3.1.33, created on 2019-09-16 02:55:41
  from 'C:\laragon\www\gym\application\modules\_admin\views\member\create.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7ef9ad281c52_15092476',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b4418eb36ed463dc8290aaa05dab9c8283c7bc64' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\member\\create.tpl',
      1 => 1568601662,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7ef9ad281c52_15092476 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13645120615d7ef9ad25fed0_44146191', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'content'} */
class Block_13645120615d7ef9ad25fed0_44146191 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_13645120615d7ef9ad25fed0_44146191',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="<?php echo base_url();?>
_admin/member/create">
        <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="" required/>
                  <?php echo form_error('name');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">email</label>
                  <input type="email" class="form-control" name="email" placeholder="Email aktif" value="" required/>
                  <?php echo form_error('email');?>

                </div>
                <div class="col-md-4">
                  <label for="exampleInputEmail1">Jenis member</label>
                  <select class="form-control" name="jenis" required/>
                    <option>--- Jenis member ---</option>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['get_member_tipe']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                      <?php ob_start();
echo $_smarty_tpl->tpl_vars['row']->value->id;
$_prefixVariable1 = ob_get_clean();
if ($_prefixVariable1 == $_smarty_tpl->tpl_vars['get_member']->value->id_member) {?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['row']->value->id;?>
" selected><?php echo $_smarty_tpl->tpl_vars['row']->value->name;?>
</option>
                      <?php } else { ?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['row']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['row']->value->name;?>
</option>
                      <?php }?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                  </select>
                  <?php echo form_error('jenis');?>

                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Password</label>
                  <input type="password" class="form-control" name="password" value="" required/>
                  <?php echo form_error('password');?>

                </div>
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Confirm Password</label>
                  <input type="password" class="form-control" name="confirm_password" value="" required/>
                  <?php echo form_error('confirm_password');?>

                </div>
              </div>
            </div>
          
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
}
