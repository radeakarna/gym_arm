<?php
/* Smarty version 3.1.33, created on 2019-11-13 00:56:53
  from 'C:\laragon\www\gym\application\views\trainer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dcb54d5c9da67_53602970',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e157fbb27e44414610cd4518ba64f702196d5495' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\views\\trainer.tpl',
      1 => 1573606611,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dcb54d5c9da67_53602970 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php echo $_smarty_tpl->tpl_vars['tittle']->value;?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13378546515dcb54d5c8c0d5_32687797', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18698274565dcb54d5c9cda5_85853352', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "layouts/index.tpl");
}
/* {block 'content'} */
class Block_13378546515dcb54d5c8c0d5_32687797 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_13378546515dcb54d5c8c0d5_32687797',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<style type="text/css">
.breadcrumb_block{
    background-color:#28a745;
}
</style>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
    <div class="container">
        <div class="row justify-content-center" style="padding-top:100px;">
            <div class="col-md-4">
                <div class="breadcrumb_inner">
                    <h3>trainer</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb_block">
        <ul>
            <li><a href="index.html">home</a></li>
            <li>trainer</li>
        </ul>
    </div>
</div>
<!--User Profile-->

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['member']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
    <div class="user_profile_wrapper clv_section">
        <div class="container">
            <div class="user_profile_section">
                <div class="profile_image_block">
                    <div class="user_profile_img">
                        <img src="<?php echo base_url();?>
assets/user/images/trainer/<?php echo $_smarty_tpl->tpl_vars['row']->value->foto;?>
" alt="image">
                    </div>
                </div>
                <div class="checkout_heading">
                    <h3>information</h3>
                </div>
                <div class="profile_form">
                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <div class="form_block">
                                <h6>Nama</h6>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <div class="form_block">
                                <span><?php echo $_smarty_tpl->tpl_vars['row']->value->nama;?>
</span>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <div class="form_block">
                                <h6>Jenis kelamin</h6>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <div class="form_block">
                                <span><?php echo $_smarty_tpl->tpl_vars['row']->value->jk;?>
</span>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <div class="form_block">
                                <h6>Alamat</h6>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <div class="form_block">
                                <span><?php echo $_smarty_tpl->tpl_vars['row']->value->alamat;?>
</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_18698274565dcb54d5c9cda5_85853352 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_18698274565dcb54d5c9cda5_85853352',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        
<?php
}
}
/* {/block 'footer'} */
}
