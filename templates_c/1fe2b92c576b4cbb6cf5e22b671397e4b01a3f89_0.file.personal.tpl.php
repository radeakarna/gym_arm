<?php
/* Smarty version 3.1.33, created on 2019-10-06 08:52:12
  from 'C:\laragon\www\gym\application\modules\_admin\views\chart\personal.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d99ab3c2b2f84_65765711',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1fe2b92c576b4cbb6cf5e22b671397e4b01a3f89' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\chart\\personal.tpl',
      1 => 1570351920,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d99ab3c2b2f84_65765711 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6470861865d99ab3c2a7379_13389970', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16805278465d99ab3c2ad260_87329304', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17677876665d99ab3c2ae272_03491617', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_6470861865d99ab3c2a7379_13389970 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_6470861865d99ab3c2a7379_13389970',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Morris Charts CSS -->
<link href="<?php echo base_url();?>
plugins/moris_chart/morris.css" rel="stylesheet">
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_16805278465d99ab3c2ad260_87329304 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_16805278465d99ab3c2ad260_87329304',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<section class="content">
    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan harian</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan bulanan</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart-bulan" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan Pertahun</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart-tahun" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_17677876665d99ab3c2ae272_03491617 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_17677876665d99ab3c2ae272_03491617',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Morris Charts JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/moris_chart/raphael.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/moris_chart/morris.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"<?php echo base_url();?>
_admin/chart_controller/get_personal_data_day",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});

$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart-bulan',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"<?php echo base_url();?>
_admin/chart_controller/get_personal_data_month",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});

$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart-tahun',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"<?php echo base_url();?>
_admin/chart_controller/get_personal_data_year",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});
<?php echo '</script'; ?>
>

<?php
}
}
/* {/block 'footer'} */
}
