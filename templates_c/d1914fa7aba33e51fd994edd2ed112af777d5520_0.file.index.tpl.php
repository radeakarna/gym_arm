<?php
/* Smarty version 3.1.33, created on 2019-09-30 05:28:21
  from 'C:\laragon\www\gym\application\modules\member\views\self\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d919275add370_27775623',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd1914fa7aba33e51fd994edd2ed112af777d5520' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\member\\views\\self\\index.tpl',
      1 => 1569593237,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d919275add370_27775623 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3306558405d9192758bcd89_37782863', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15200570545d9192758c2662_50923277', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_341894325d919275adb779_38810887', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_3306558405d9192758bcd89_37782863 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_3306558405d9192758bcd89_37782863',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<link rel="stylesheet" href="<?php echo base_url();?>
plugins/datetimepicker/css/bootstrap-datepicker.css">
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_15200570545d9192758c2662_50923277 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_15200570545d9192758c2662_50923277',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['getmember']->value->id)) {?>
  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">data diri anda</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          
          <div class="box-body">
            <form method="post" class="form-horizontal" action="<?php echo base_url();?>
member/self_controller/update/<?php echo $_smarty_tpl->tpl_vars['getmember']->value->id;?>
">
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">No KTP</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->nisn;?>
</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Tempat Lahir</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->place_of_birth;?>
</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Tanggal Lahir</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->date_of_birth;?>
</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Nomor telepon</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->telp;?>
</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Tinggi badan</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->hight;?>
</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Berat badan</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->wight;?>
</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Jenis kelamin</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->sex;?>
</h5>
              </div>
            </div>
            <div class="form-group" style="border-bottom:solid 1px #00a613; width: 98%; margin:auto">
              <label class="col-sm-3 control-label">Alamat</label>
              <div class="col-sm-9">
                <h5 style="color:#00a613;font-weight:bold"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->addres;?>
</h5>
              </div>
            </div>
            
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <!-- <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->nisn;?>
" required/>
                  <?php echo form_error('nik');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->place_of_birth;?>
" required/>
                  <?php echo form_error('tmp_lahir');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->date_of_birth;?>
" required/>
                  <?php echo form_error('tgl_lahir');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->telp;?>
" required/>
                  <?php echo form_error('no_telepon');?>

                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tinggi badan</label>
                  <input type="text" class="form-control" name="tinggi" placeholder="Tinggi basan" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->hight;?>
" required/>
                  <?php echo form_error('tinggi');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Berat badan</label>
                  <input type="text" class="form-control" name="berat" placeholder="Berat badan" value="<?php echo $_smarty_tpl->tpl_vars['getmember']->value->wight;?>
" required/>
                  <?php echo form_error('berat');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  <?php echo form_error('jk');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4"><?php echo $_smarty_tpl->tpl_vars['getmember']->value->addres;?>
</textarea>
                  <?php echo form_error('alamat');?>

                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="jumbotron" style="padding:10px;">
                <h5>Tinggi badan = <?php echo $_smarty_tpl->tpl_vars['getmember']->value->hight;?>
 cm</h5>
                <h5>Berat badan = <?php echo $_smarty_tpl->tpl_vars['getmember']->value->wight;?>
 kg</h5>
                <h5>Umur saat ini = <?php echo hitung_umur($_smarty_tpl->tpl_vars['getmember']->value->date_of_birth);?>
 tahun</h5>
                <span><h4>TDEE anda adalah <?php echo tdee($_smarty_tpl->tpl_vars['getmember']->value->sex,$_smarty_tpl->tpl_vars['getmember']->value->wight,$_smarty_tpl->tpl_vars['getmember']->value->hight,hitung_umur($_smarty_tpl->tpl_vars['getmember']->value->date_of_birth));?>
 Kalori/perhari</h4></span></div>
            </div> -->
            </form>
          </div>
          <div class="box-footer clearfix">
            <!-- <button type="submit" class="btn btn-primary">Simpan data</button> -->
          </div>
          
      </div>

  </section><!-- /.content -->

  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">Latihan yang sudah dilakukan</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          
          <div class="box-body">

            <table class="table table-striped">
              <tr>
                <th style="width: 10px">#</th>
                <th>Jenis latihan</th>
                <th>Tanggal latiha</th>
                <!-- <th style="width: 40px">delete</th> -->
              </tr>
              <?php $_smarty_tpl->_assignInScope('increment', 1);?>
              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['exercise']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
              <tr>
                <td class="td"  align="center"><?php echo $_smarty_tpl->tpl_vars['increment']->value;?>
.</td>
                <td class="td" ><?php echo $_smarty_tpl->tpl_vars['row']->value->exercise;?>
</td>
                <td>
                  <?php echo tgl_indo($_smarty_tpl->tpl_vars['row']->value->date_exercise);?>

                </td>
                <!-- <td><span class="badge bg-red"><i class="fa fa-trash-o"></i></span></td> -->
              </tr>
              <?php $_smarty_tpl->_assignInScope('increment', $_smarty_tpl->tpl_vars['increment']->value+1);?>
              <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              
            </table>

            <br><br><hr>
            <form method="post" action="<?php echo base_url();?>
member/self_controller/store_excercise">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Latihan dilakukan</label>
                  <textarea name="latihan" class="form-control" rows="4"></textarea>
                  <?php echo form_error('latihan');?>

                </div>
                <div class="col-md-6">
                  <label for="exampleInputEmail1">Tanggal Latihan</label>
                  <input type="text" class="form-control" id="datepicker_two" name="tanggal_latihan" placeholder="Tanggal pada saat latihan" value="" required/>
                  <?php echo form_error('tanggal_latihan');?>

                </div>
              </div>
            </div>
          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary">Simpan data</button>
          </div>
          </form>
      </div>

  </section><!-- /.content -->
<?php } else { ?>
  <?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-info">
        <h4>INFO!</h4>
        <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
      </div>
    </section>
  <?php } else { ?>
    <section class="content-header">
      <div class="callout callout-danger">
        <h4>Perhatian!</h4>
        <p>Mohon lengkapi data diri terlebih dahulu.</p>
      </div>
    </section>
  <?php }?>
  <section class="content">
      <!-- quick email widget -->
      <div class="box box-success">
          <div class="box-header">
              <i class="fa fa-user"></i>
              <h3 class="box-title">Melengkapi data diri</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                  
              </div><!-- /. tools -->
          </div>
          <form method="post" action="<?php echo base_url();?>
member/self/">
          <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">No KTP</label>
                  <input type="text" class="form-control" name="nik" placeholder="No KTP" value="" required/>
                  <?php echo form_error('nik');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tempat Lahir</label>
                  <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" value="" required/>
                  <?php echo form_error('tmp_lahir');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tanggal Lahir</label>
                  <input type="text" class="form-control" id="datepicker" name="tgl_lahir" placeholder="Tanggal lahir:" value="" required/>
                  <?php echo form_error('tgl_lahir');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Nomor telepon</label>
                  <input type="telp" class="form-control" name="no_telepon" placeholder="Nomor telopon aktif:" required/>
                  <?php echo form_error('no_telepon');?>

                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Tinggi badan</label>
                  <input type="text" class="form-control" name="tinggi" placeholder="Tinggi basan" value="" required/>
                  <?php echo form_error('tinggi');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Berat badan</label>
                  <input type="text" class="form-control" name="berat" placeholder="Berat badan" value="" required/>
                  <?php echo form_error('berat');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Jenis kelamin</label>
                  <select name="jk" class="form-control">
                    <option value="laki-laki">laki-laki</option>
                    <option value="perempuan">perempuan</option>
                  </select>
                  <?php echo form_error('jk');?>

                </div>
                <div class="col-md-3">
                  <label for="exampleInputEmail1">Alamat</label>
                  <textarea name="alamat" class="form-control" rows="4"></textarea>
                  <?php echo form_error('alamat');?>

                </div>
              </div>
            </div>
          </div>
          <div class="box-footer clearfix">
            <button type="submit" class="btn btn-primary">Simpan data</button>
          </div>
          </form>
      </div>

  </section><!-- /.content -->
<?php }?>

<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_341894325d919275adb779_38810887 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_341894325d919275adb779_38810887',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Datetimepicker -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/datetimepicker/js/bootstrap-datepicker.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    //function get_tanggal(){
        $(function () {
            $('#datepicker').datepicker({
              startView: 2,
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });    

        $(function () {
            $('#datepicker_two').datepicker({
              language: "id",
              orientation: "bottom auto",
              autoclose: true,
              format: 'yyyy-mm-dd',
            });
        });    
    //}      
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'footer'} */
}
