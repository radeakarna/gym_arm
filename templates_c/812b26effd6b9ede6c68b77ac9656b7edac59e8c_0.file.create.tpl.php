<?php
/* Smarty version 3.1.33, created on 2019-09-13 15:59:41
  from 'C:\laragon\www\gym\application\modules\_admin\views\pembayaran\create.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7bbcedb1fea8_59073750',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '812b26effd6b9ede6c68b77ac9656b7edac59e8c' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\pembayaran\\create.tpl',
      1 => 1568390379,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7bbcedb1fea8_59073750 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17912245655d7bbcedb0b952_34979547', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'content'} */
class Block_17912245655d7bbcedb0b952_34979547 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_17912245655d7bbcedb0b952_34979547',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="<?php echo base_url();?>
_admin/payment/create">
        <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="" required/>
                <?php echo form_error('name');?>

            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">ID member</label>
                <input type="text" class="form-control" name="id_member" placeholder="Kode member" value="" required/>
                <?php echo form_error('name');?>

            </div>
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
}
