<?php
/* Smarty version 3.1.33, created on 2019-10-05 11:41:46
  from 'C:\laragon\www\gym\application\modules\_admin\views\presensi\personal\create.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d98817ac03a81_69872082',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9706878bcf56709783f1fcf403d53377f4a5d88f' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\presensi\\personal\\create.tpl',
      1 => 1570275704,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d98817ac03a81_69872082 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_799478435d98817aae3839_92396839', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10535197765d98817ac02ee1_86122908', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'content'} */
class Block_799478435d98817aae3839_92396839 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_799478435d98817aae3839_92396839',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Presensi personal member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="<?php echo base_url();?>
_admin/personal_presences/create">
        <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="input-group">
              <input type="text" class="form-control" name="id_member" id="id_member" placeholder="Code member" value="" required/>
              <span class="input-group-btn">
                <button type="submit" class="btn btn-success btn-flat">Presensi</button>
              </span>
            </div>
        </div>
        <div class="box-footer clearfix">
          <!-- <button type="submit" class="btn btn-primary">Simpan data</button> -->
        </div>
        </form>
    </div>
</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_10535197765d98817ac02ee1_86122908 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_10535197765d98817ac02ee1_86122908',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php
}
}
/* {/block 'footer'} */
}
