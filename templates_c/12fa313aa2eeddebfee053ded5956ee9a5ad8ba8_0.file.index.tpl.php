<?php
/* Smarty version 3.1.33, created on 2019-11-13 00:53:21
  from 'C:\laragon\www\gym\application\views\layouts\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dcb5401506204_45563169',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '12fa313aa2eeddebfee053ded5956ee9a5ad8ba8' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\views\\layouts\\index.tpl',
      1 => 1573606397,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dcb5401506204_45563169 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<!DOCTYPE html>
<!-- 
Template Name: Cultivation - Multipurpose Responsive HTML Template
Version: 1.0.0
Author: Kamleshyadav
Website: 
Purchase: 
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zxx">
<!--<![endif]-->
<!-- Begin Head --> 
<head>
    <title><?php echo (($tmp = @$_smarty_tpl->tpl_vars['tittle']->value)===null||$tmp==='' ? 'Default Page Title' : $tmp);?>
</title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="kamleshyadav">
    <meta name="MobileOptimized" content="320">
    <!--Start Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/font.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/range.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/nice-select.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>
assets/user/css/style.css">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16108758905dcb54014c48a2_90683702', 'header');
?>

    <!-- Favicon Link -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>
assets/user/images/favicon.png">
</head>
<body>
	<!-- <div class="preloader_wrapper">
		<div class="preloader_inner">
			<img src="<?php echo base_url();?>
assets/user/images/loading/preloader.gif" alt="image" />
		</div>
	</div> -->
<div class="clv_main_wrapper index_v4">
	<div class="header3_wrapper">
		<div class="clv_header3">
			<div class="row">
				<div class="col-lg-2 col-md-2">
					<div class="clv_left_header">
						<div class="clv_logo">
							<a href="<?php echo base_url();?>
"><img src="<?php echo base_url();?>
assets/user/images/logo4.png" alt="Cultivation" /></a>
						</div>
					</div>
				</div>
				<div class="col-lg-10 col-md-10">
					<div class="clv_right_header">
						<div class="clv_menu">
							<div class="clv_menu_nav">
								<ul>
									<li>
										<a href="<?php echo base_url();?>
">home</a>
									</li>
									<li><a href="<?php echo base_url();?>
pages/about">about us</a></li>
									<li><a href="<?php echo base_url();?>
pages/trainer">trainer</a></li>
									<li><a href="<?php echo base_url();?>
pages/contact">contact us</a></li>
								</ul>
							</div>
							<!-- <div class="cart_nav">
								<ul>
									<li>
										<a class="search_toggle" href="javascript:;"><i class="fa fa-search" aria-hidden="true"></i></a>
									</li>
								</ul>
							</div> -->
							<!-- <a href="<?php echo base_url();?>
pages/signup" class="clv_btn">Member</a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10350659825dcb54014ca8a7_98332956', 'content');
?>

	<!--Footer-->
	<div class="clv_footer_wrapper clv_section">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-lg-4">
					<div class="footer_block">
						<div class="footer_logo"><a href="javascript:;"><img src="<?php echo base_url();?>
assets/user/images/footer_logo4.png" alt="image" /></a></div>
						<p><span><svg 
								 xmlns="http://www.w3.org/2000/svg"
								 xmlns:xlink="http://www.w3.org/1999/xlink"
								 width="16px" height="16px">
								<defs>
								<filter id="Filter_0">
									<feFlood flood-color="rgb(31, 161, 46)" flood-opacity="1" result="floodOut" />
									<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
									<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
								</filter>

								</defs>
								<g filter="url(#Filter_0)">
								<path fill-rule="evenodd"  fill="rgb(81, 176, 30)"
								 d="M14.873,0.856 C14.815,0.856 14.700,0.856 14.643,0.913 L0.850,6.660 C0.620,6.776 0.505,6.948 0.505,7.176 C0.505,7.465 0.677,7.695 0.965,7.752 L6.942,9.189 C7.057,9.189 7.114,9.305 7.172,9.419 L8.608,15.396 C8.666,15.626 8.896,15.855 9.183,15.855 C9.413,15.855 9.643,15.683 9.700,15.511 L15.447,1.718 C15.447,1.660 15.505,1.603 15.505,1.488 C15.447,1.085 15.217,0.856 14.873,0.856 ZM9.355,8.902 L9.068,7.695 C9.011,7.465 8.838,7.350 8.666,7.292 L7.459,7.005 C7.172,6.948 7.172,6.545 7.401,6.487 L11.022,4.993 C11.252,4.878 11.482,5.109 11.424,5.395 L9.930,9.017 C9.758,9.189 9.413,9.131 9.355,8.902 Z"/>
								</g>
								</svg></span> <?php echo get_metsos('addres_gym');?>
</p>
						<p><span><svg 
								 xmlns="http://www.w3.org/2000/svg"
								 xmlns:xlink="http://www.w3.org/1999/xlink"
								 width="16px" height="15px">
								<defs>
								<filter id="Filter_0">
									<feFlood flood-color="rgb(31, 161, 46)" flood-opacity="1" result="floodOut" />
									<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
									<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
								</filter>

								</defs>
								<g filter="url(#Filter_0)">
								<path fill-rule="evenodd"  fill="rgb(81, 176, 30)"
								 d="M13.866,7.235 C13.607,5.721 12.892,4.344 11.802,3.254 C10.653,2.108 9.197,1.381 7.592,1.156 L7.755,-0.002 C9.613,0.257 11.296,1.096 12.626,2.427 C13.888,3.692 14.716,5.284 15.019,7.039 L13.866,7.235 ZM10.537,4.459 C11.296,5.222 11.796,6.181 11.977,7.238 L10.824,7.436 C10.684,6.617 10.300,5.874 9.713,5.287 C9.091,4.666 8.304,4.276 7.439,4.155 L7.601,2.996 C8.719,3.151 9.734,3.657 10.537,4.459 ZM4.909,8.182 C5.709,9.162 6.611,10.033 7.689,10.711 C7.920,10.854 8.176,10.960 8.417,11.092 C8.538,11.160 8.623,11.139 8.723,11.035 C9.088,10.661 9.460,10.293 9.831,9.924 C10.318,9.440 10.931,9.440 11.421,9.924 C12.017,10.516 12.614,11.110 13.207,11.707 C13.704,12.207 13.701,12.818 13.201,13.324 C12.864,13.665 12.505,13.989 12.186,14.345 C11.721,14.866 11.140,15.035 10.472,14.997 C9.500,14.944 8.607,14.623 7.745,14.205 C5.831,13.275 4.194,11.985 2.823,10.355 C1.808,9.150 0.971,7.834 0.422,6.355 C0.153,5.639 -0.038,4.906 0.022,4.129 C0.059,3.651 0.237,3.242 0.590,2.907 C0.971,2.546 1.330,2.168 1.705,1.800 C2.192,1.319 2.804,1.319 3.295,1.797 C3.598,2.093 3.894,2.396 4.194,2.696 C4.485,2.988 4.775,3.277 5.065,3.570 C5.578,4.085 5.578,4.684 5.069,5.197 C4.703,5.565 4.341,5.933 3.969,6.293 C3.873,6.390 3.863,6.468 3.913,6.586 C4.160,7.173 4.513,7.694 4.909,8.182 Z"/>
								</g>
								</svg></span> <?php echo get_metsos('contact');?>
</p>
						<p><span><svg 
							 xmlns="http://www.w3.org/2000/svg"
							 xmlns:xlink="http://www.w3.org/1999/xlink"
							 width="16px" height="16px">
							<defs>
							<filter id="Filter_0">
								<feFlood flood-color="rgb(31, 161, 46)" flood-opacity="1" result="floodOut" />
								<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
								<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
							</filter>

							</defs>
							<g filter="url(#Filter_0)">
							<path fill-rule="evenodd"  fill="rgb(81, 176, 30)"
							 d="M16.000,5.535 C16.000,4.982 15.680,4.507 15.280,4.191 L8.000,-0.002 L0.720,4.191 C0.320,4.507 0.000,4.982 0.000,5.535 L0.000,13.447 C0.000,14.317 0.720,15.028 1.600,15.028 L14.400,15.028 C15.280,15.028 16.000,14.317 16.000,13.447 L16.000,5.535 ZM8.000,9.491 L1.360,5.376 L8.000,1.579 L14.640,5.376 L8.000,9.491 Z"/>
							</g>
							</svg></span> <?php echo get_metsos('gmail');?>
</p>
						<ul class="agri_social_links">
							<li><a href="<?php echo get_metsos('facebook');?>
"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a></li>
							<li><a href="<?php echo get_metsos('twitter');?>
"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a></li>
							<li><a href="<?php echo get_metsos('instagram');?>
"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 col-lg-4">
					<div class="footer_block">
						<div class="footer_heading">
							<h4>Membership</h4>
							<img src="<?php echo base_url();?>
assets/user/images/garden_underline3.png" alt="image" />
						</div>
						<div class="footer_post_section">
							<div class="footer_post_slide">
								<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
								<div class="blog_links">
									<p>individu</p>
								</div>
							</div>
							<div class="footer_post_slide">
								<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
								<div class="blog_links">
									<p>Personal</p>
								</div>
							</div>
							<div class="footer_post_slide">
								<span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
								<div class="blog_links">
									<p>Personal trainer</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-lg-4">
					<div class="footer_block">
						<div class="footer_heading">
							<h4>about us</h4>
							<img src="<?php echo base_url();?>
assets/user/images/garden_underline3.png" alt="image" />
						</div>
						<a href="<?php echo base_url();?>
pages/about">
						<?php echo truncate(get_metsos('about'),200);?>

						</a>
						<!-- <ul class="instagram_links">
							<li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
                            <li><a href="javascript:;"><img src="https://via.placeholder.com/83x83" alt="image" /></a></li>
						</ul> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Copyright-->
	<div class="clv_copyright_wrapper">
		<p>copyright &copy; 2019</p>
	</div>

	<!--Popup-->
	<div class="search_box">
		<div class="search_block">
			<h3>Explore more with us</h3>
			<div class="search_field">
				<input type="search" placeholder="Search Here" />
				<a href="javascript:;">search</a>
			</div>
		</div>
		<span class="search_close">
			<?php echo '<?xml ';?>version="1.0" encoding="iso-8859-1"<?php echo '?>';?>
			<svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"  width="30px" height="30px">
			<g>
				<path style="fill:#2a7d2e;" d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
					c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
					C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
					s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/>
			</g>
			</svg>
		</span>
	</div>

    <!--SignUp Popup-->
    <!-- <div class="signup_wrapper">
        <div class="signup_inner">
            <div class="signup_details">
                <div class="site_logo">
                    <a href="index.html"> <img src="<?php echo base_url();?>
assets/user/images/logo_white.png" alt="image"></a>
                </div>
                <h3>welcome to cultivation!</h3>
                <p>Consectetur adipisicing elit sed do eiusmod por incididunt uttelabore et dolore magna aliqu.</p>
                <a href="javascript:;" class="clv_btn white_btn pop_signin">sign in</a>
            </div>
            <div class="signup_form_section">
                <h4>create account</h4>
                <img src="<?php echo base_url();?>
assets/user/images/clv_underline.png" alt="image">
                <form id="myForm" method="POST">
                	<input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                	<div class="form_block">
	                    <input type="text" class="form_field" placeholder="name" name="name" required/>
	                    <span id="error_name"></span>
	                </div>
	                <div class="form_block">
	                    <input type="email" class="form_field" placeholder="Email" name="email" required/>
	                    <span id="error_email"></span>
	                </div>
	                <div class="form_block">
	                    <input type="password" class="form_field" placeholder="Password" name="password" required/>
	                    <span id="error_password"></span>
	                </div>
	                <div class="form_block">
	                    <input type="password" class="form_field" placeholder="Confirm password" name="confirm_password" required/>
	                    <span id="error_confirm_password"></span>
	                </div>
	                <div class="form_block">
	                    <select class="form_field" name="jenis" required/>
	                    	<option>--- Jenis member ---</option>
	                    	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['member']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
	                    	<option value='<?php echo $_smarty_tpl->tpl_vars['row']->value->id;?>
'><?php echo $_smarty_tpl->tpl_vars['row']->value->name;?>
</option>
	                    	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	                    </select>
	                    <span id="error_type"></span>
	                </div>
	                <div class="form_block" style="top:20px !important;clear:both">
	                </div>
	                <div class="row">
	                	<div class="col-md-5">
		            		<div class="form_block">
		            		<p id='getcapcha'><?php echo $_smarty_tpl->tpl_vars['getCaptcha']->value;?>
</p>
		            		</div>
		            	</div>
		            	<div class="col-md-7">
		            		<div class="form_block">
		            		<input type="text" name="captcha" class="form_field"  placeholder="captcha" required/>
		            		<span id="error_captcha"></span>
					     	</div>
		            	</div>
	                </div>
	                <div class="form_block">
	                	<button type="button" onclick="insert()" class="clv_btn submitForm" value="signin" data-type="contact">sign up</button>
	             	</div>
                </form>
                <span class="success_close">
                    <?php echo '<?xml ';?>version="1.0" encoding="iso-8859-1"<?php echo '?>';?>
                    <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
                    <g>
                        <path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
                        c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
                        l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
                        c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
                    </g>
                    </svg>
                </span>
            </div>
        </div>
    </div> -->
    <!--SignIn Popup-->
    <!-- <div class="signin_wrapper">
        <div class="signup_inner">
            <div class="signup_details">
                <div class="site_logo">
                    <a href="index.html"> <img src="<?php echo base_url();?>
assets/user/images/logo_white.png" alt="image"></a>
                </div>
                <h3>welcome to ARM Fitnes center</h3>
                <p>Silahkan daftarkan diri anda memjadi member terlebih dahulu untuk memulai latihan.</p> -->
                <!-- <a href="javascript:;" class="clv_btn white_btn pop_signup">sign up</a> -->
                <!-- <a href="<?php echo base_url();?>
pages/signup" class="clv_btn white_btn pop_signup">sign up</a>
            </div>
            <div class="signup_form_section">
                <h4>sign in account</h4>
                <img src="<?php echo base_url();?>
assets/user/images/clv_underline.png" alt="image">
                <form id="find" method="POST">
                	<input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" id='login_token' value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
	                <div class="form_block">
	                    <input type="text" class="form_field" name="email" placeholder="Email" required/>
	                    <span id="error_find_email"></span>
	                </div>
	                <div class="form_block">
	                    <input type="password" class="form_field" name="password" placeholder="Password" required/>
	                    <span id="error_find_password"></span>
	                </div>
	                <button type="button" onclick="find()" class="clv_btn submitForm" value="signin" data-type="contact">sign in</button>
            	</form>
                <span class="success_close">
                    <?php echo '<?xml ';?>version="1.0" encoding="iso-8859-1"<?php echo '?>';?>
                    <svg version="1.1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 212.982 212.982" style="enable-background:new 0 0 212.982 212.982;" xml:space="preserve" width="11px" height="11px" >
                    <g>
                        <path fill="#fec007" style="fill-rule:evenodd;clip-rule:evenodd;" d="M131.804,106.491l75.936-75.936c6.99-6.99,6.99-18.323,0-25.312
                        c-6.99-6.99-18.322-6.99-25.312,0l-75.937,75.937L30.554,5.242c-6.99-6.99-18.322-6.99-25.312,0c-6.989,6.99-6.989,18.323,0,25.312
                        l75.937,75.936L5.242,182.427c-6.989,6.99-6.989,18.323,0,25.312c6.99,6.99,18.322,6.99,25.312,0l75.937-75.937l75.937,75.937
                        c6.989,6.99,18.322,6.99,25.312,0c6.99-6.99,6.99-18.322,0-25.312L131.804,106.491z"/>
                    </g>
                    </svg>
                </span>
            </div>
        </div>
    </div> -->
    <!--Profile Toggle-->
    <!-- <div class="profile_toggle"><a href="javascript:;"><img src="<?php echo base_url();?>
assets/user/images/login.gif" alt=""></a></div> -->
</div>
<!--Main js file Style-->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/bootstrap.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/swiper.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/magnific-popup.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/jquery.themepunch.tools.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/jquery.themepunch.revolution.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/jquery.appear.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/jquery.countTo.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/isotope.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/nice-select.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/range.js"><?php echo '</script'; ?>
>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->	
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/revolution.extension.actions.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/revolution.extension.kenburn.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/revolution.extension.layeranimation.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/revolution.extension.migration.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/revolution.extension.parallax.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/revolution.extension.slideanims.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/revolution.extension.video.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/user/js/custom.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
function insert() {
	var formData = new FormData($('#myForm')[0]);
	var store = '<?php echo base_url();?>
registration/store';
	var csrf = '<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
';
	//console.log(csrf);
    $.ajax({
      url: store,
      data : formData,
      type:'post',
      contentType: false,
      processData: false,
      dataType: 'JSON',
      beforeSend:function(){
        //$('#ayogaskan').text("proses...");
      },
      success:function(data){
      	if (data.status) {
      		// $('input[name=csrf_test_name]').val(data.token);
      		$('#signin_token').val(data.token);
      		$('#login_token').val(data.token);
	    	$('input[name=name]').val('');
	    	// $('input[name=type]').val('');
	    	$('input[name=email]').val('');
	    	$('input[name=password]').val('');
	    	$('input[name=confirm_password]').val('');
	    	$('input[name=captcha]').val('');
	    	$('#getcapcha').html(data.getCaptcha);
	    	//console.log(data.token);
	    	$('#error_name').html('');
	    	$('#error_type').html('');
	    	$('#error_email').html('');
	    	$('#error_password').html('');
	    	$('#error_confirm_password').html('');
	    	$('#error_captcha').html('');

	    	alert('data berhasil diinputkan');
      	}else{
      		// $('input[name=csrf_test_name]').val(data.token);
      		$('#signin_token').val(data.token);
      		$('#login_token').val(data.token);
      		//console.log(data);
	    	$('#error_name').html(data.name);
	    	$('#error_email').html(data.email);
	    	$('#error_password').html(data.password);
	    	$('#error_confirm_password').html(data.confirm_password);
	    	$('#error_captcha').html(data.captcha);
	    	$('#error_type').html(data.jenis);
      	}
      },
      error: function(jqXHR, textStatus, errorThrown){
        console.log(errorThrown);
      }
    });
}
function find() {
	var formData = new FormData($('#find')[0]);
	var store = '<?php echo base_url();?>
Auth_Controller/find';
    $.ajax({
      url: store,
      data : formData,
      type:'post',
      contentType: false,
      processData: false,
      dataType: 'JSON',
      beforeSend:function(){
        //$('#ayogaskan').text("proses...");
      },
      success:function(data){
      	//console.log(data);
      	if (data.status) {
      		// $('input[name=csrf_test_name]').val(data.token);
      		$('#signin_token').val(data.token);
      		$('#login_token').val(data.token);
      		$('#error_find_email').html('');
	    	$('#error_find_password').html('');
	    	window.location.replace(data.result);
      	}else{
      		// $('input[name=csrf_test_name]').val(data.token);
      		$('#signin_token').val(data.token);
      		$('#login_token').val(data.token);
      		$('#error_find_email').html(data.email);
	    	$('#error_find_password').html(data.password);
      	}
      },
      error: function(jqXHR, textStatus, errorThrown){
        console.log(errorThrown);
      }
    });
}
<?php echo '</script'; ?>
>
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10092659405dcb54015042c9_75979733', 'footer');
?>

</body>
</html><?php }
/* {block 'header'} */
class Block_16108758905dcb54014c48a2_90683702 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_16108758905dcb54014c48a2_90683702',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_10350659825dcb54014ca8a7_98332956 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_10350659825dcb54014ca8a7_98332956',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<!--Banner Slider-->
	<div class="clv_banner_slider">
		<!-- Swiper -->
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide" style="background:url('assets/user/images/hrader-1.jpg') !important">
					<div class="clv_slide">
						<div class="container">
							<div class="clv_slide_inner">
								<h1>ARM</h1>
								<h2>Fitnes center</h2>
								<p><?php echo get_metsos('addres_gym');?>
</p>
								<!-- <div class="banner_btn">
									<span class="left_line"></span>
									<a href="javascript:;" class="clv_btn">discover more</a>
									<span class="right_line"></span>
								</div> -->
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<!-- Add Arrows -->
			<span class="slider_arrow banner_left left_arrow">
				<svg 
				 xmlns="http://www.w3.org/2000/svg"
				 xmlns:xlink="http://www.w3.org/1999/xlink"
				 width="10px" height="20px">
				<path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
				 d="M0.272,10.703 L8.434,19.703 C8.792,20.095 9.372,20.095 9.731,19.703 C10.089,19.308 10.089,18.668 9.731,18.274 L2.217,9.990 L9.730,1.706 C10.089,1.310 10.089,0.672 9.730,0.277 C9.372,-0.118 8.791,-0.118 8.433,0.277 L0.271,9.274 C-0.082,9.666 -0.082,10.315 0.272,10.703 Z"/>
				</svg>
			</span>
			<span class="slider_arrow banner_right right_arrow">
				<svg 
				 xmlns="http://www.w3.org/2000/svg"
				 xmlns:xlink="http://www.w3.org/1999/xlink"
				 width="10px" height="20px">
				<path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
				 d="M9.728,10.703 L1.566,19.703 C1.208,20.095 0.627,20.095 0.268,19.703 C-0.090,19.308 -0.090,18.668 0.268,18.274 L7.783,9.990 L0.269,1.706 C-0.089,1.310 -0.089,0.672 0.269,0.277 C0.627,-0.118 1.209,-0.118 1.567,0.277 L9.729,9.274 C10.081,9.666 10.081,10.315 9.728,10.703 Z"/>
				</svg>
			</span>
		</div>
	</div>
	<!--About-->
	<div class="garden_about_wrapper clv_section">
		<div class="container">
			<!--Service-->
			<div class="garden_service_wrapper">
				<div class="row">
					<div class="col-lg-2 col-md-2">
						
					</div>
					<div class="col-lg-8 col-md-8">
						
					</div>
					<div class="col-lg-2 col-md-2">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="garden_pricing_wrapper clv_section" stye="background-color:blue !important">
		<div class="container">
			<div class="garden_about_section">
				<div class='col-md-12 col-lg-6 offset-md-3'>
					<div class="contact_form_section" style="border:solid 1px #e0e0e0">
	                    <div class="row">
	                        <div class="col-md-12 col-lg-12 d-flex justify-content-center">
	                            <h3 >Login</h3>
	                        </div>
							<form id="find" method="post" action="<?php echo base_url();?>
auth_controller/find">
							<input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" id='login_token' value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
	                        <div class="col-md-12 col-lg-12">
	                            <div class="form_block">
	                                <input type="text" class="form_field" name="email" placeholder="Email" required/>
				                    <span id="error_find_email"></span>
	                            </div>
	                        </div>
	                        <div class="col-md-12 col-lg-12">
	                            <div class="form_block">
	                                <input type="password" class="form_field" name="password" placeholder="Password" required/>
				                    <span id="error_find_password"></span>
	                            </div>
	                        </div>
	                        <div class="col-md-12 col-lg-12 d-flex justify-content-center">
	                            <div class="form_block d-flex justify-content-center">
	                                <button type="submit" class="clv_btn submitForm" value="signin" data-type="contact">sign in</button>
	                            </div>
	                        </div>
							</form>

	                    </div>
	                </div>
				</div>
			</div>
		</div>
	</div>


	<!--Pricing Table-->
	<div class="garden_pricing_wrapper clv_section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-6">
					<div class="clv_heading">
						<h3>Member</h3>
						<div class="clv_underline"><img src="<?php echo base_url();?>
assets/user/images/garden_underline.png" alt="image" /></div>
						<p>Member yang tersedian di ARM GYM.</p>
					</div>
				</div>
			</div>
			<div class="pricing_section">
				<div class="row">
					<div class="col-lg-4 col-md-4">
						<div class="pricing_block">
							<div class="pricing_header" style="width:60%">
								<h3>Individu</h3>
							</div>
							<h1><span><?php echo rupiah(get_metsos('member_day'));?>
</span></h1>
							<ul>
								<li>
									<p>Pembayaran Dilakukan perhari</p>
								</li>
								<li>
									<p>Tanpa Trainer</p>
								</li>
							</ul>
							
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="pricing_block">
							<div class="pricing_header premium" style="width:60%">
								<h3>Personal</h3>
							</div>
							<h1><span><?php echo rupiah(get_metsos('personal'));?>
</span></h1>
							<ul>
								<li>
									<p>Pembayaran Dilakukan perbulan</p>
								</li>
								<li>
									<p>Tanpa Trainer</p>
								</li>
							</ul>
							
						</div>
					</div>
					<div class="col-lg-4 col-md-4">
						<div class="pricing_block">
							<div class="pricing_header ultimate" style="width:60%">
								<h3>Personal trainer</h3>
							</div>
							<h1><span><?php echo rupiah(get_metsos('personal trainer'));?>
</span></h1>
							<ul>
								<li>
									<p>Pembayaran Dilakukan perbulan</p>
								</li>
								<li>
									<p>Include Trainer</p>
								</li>
							</ul>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_10092659405dcb54015042c9_75979733 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_10092659405dcb54015042c9_75979733',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php
}
}
/* {/block 'footer'} */
}
