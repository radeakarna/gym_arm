<?php
/* Smarty version 3.1.33, created on 2019-09-16 02:05:37
  from 'C:\laragon\www\gym\application\views\signup.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7eedf10a5944_75872287',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '00d62d291747d6c8b2fa0079204cfeaa5a8c8ad2' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\views\\signup.tpl',
      1 => 1568599527,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7eedf10a5944_75872287 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php echo $_smarty_tpl->tpl_vars['tittle']->value;?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_994833045d7eedf108e844_91655739', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "layouts/index.tpl");
}
/* {block 'content'} */
class Block_994833045d7eedf108e844_91655739 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_994833045d7eedf108e844_91655739',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<style type="text/css">
.breadcrumb_block{
    background-color:#28a745;
}
</style>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
    <div class="container">
        <div class="row justify-content-center" style="padding-top:100px;">
            <div class="col-md-4">
                <div class="breadcrumb_inner">
                    <h3>Member</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb_block">
        <ul>
            <li><a href="index.html">home</a></li>
            <li>member</li>
        </ul>
    </div>
</div>
<!--Blog With Sidebar-->
<div class="blog_sidebar_wrapper clv_section blog_single_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="blog_left_section">
                    <div class="blog_section">
                        <div class="agri_blog_image">
                            <!-- <img src="https://via.placeholder.com/870x410" alt="image"> -->
                            <!-- <span class="agri_blog_date">About us</span> -->
                        </div>
                        <div class="agri_blog_content">
                            <div class="checkout_heading">
                                <h3>basic information</h3>
                            </div>
                            <div class="profile_form">
                                <div class="row">
                                    <form id="myForm" method="POST">
                                        <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" id="signin_token" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                                        <div class="form_block">
                                            <input type="text" class="form_field" placeholder="name" name="name" required/>
                                            <span id="error_name"></span>
                                        </div>
                                        <div class="form_block">
                                            <input type="email" class="form_field" placeholder="Email" name="email" required/>
                                            <span id="error_email"></span>
                                        </div>
                                        <div class="form_block">
                                            <input type="password" class="form_field" placeholder="Password" name="password" required/>
                                            <span id="error_password"></span>
                                        </div>
                                        <div class="form_block">
                                            <input type="password" class="form_field" placeholder="Confirm password" name="confirm_password" required/>
                                            <span id="error_confirm_password"></span>
                                        </div>
                                        <div class="form_block">
                                            <select class="form_field" name="jenis" required/>
                                                <option>--- Jenis member ---</option>
                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['member']->value, 'row');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
?>
                                                <option value='<?php echo $_smarty_tpl->tpl_vars['row']->value->id;?>
'><?php echo $_smarty_tpl->tpl_vars['row']->value->name;?>
</option>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                            </select>
                                            <span id="error_type"></span>
                                        </div>
                                        <div class="form_block" style="top:20px !important;clear:both">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form_block">
                                                <p id='getcapcha'><?php echo $_smarty_tpl->tpl_vars['getCaptcha']->value;?>
</p>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form_block">
                                                <input type="text" name="captcha" class="form_field"  placeholder="captcha" required/>
                                                <span id="error_captcha"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form_block">
                                            <button type="button" onclick="insert()" class="clv_btn submitForm" value="signin" data-type="contact">sign up</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<?php
}
}
/* {/block 'content'} */
}
