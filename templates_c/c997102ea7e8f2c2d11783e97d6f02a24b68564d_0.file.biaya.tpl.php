<?php
/* Smarty version 3.1.33, created on 2019-09-15 11:40:42
  from 'C:\laragon\www\gym\application\modules\_admin\views\settings\biaya.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7e233acdcb29_89351149',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c997102ea7e8f2c2d11783e97d6f02a24b68564d' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\settings\\biaya.tpl',
      1 => 1568547638,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7e233acdcb29_89351149 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2557905545d7e233acc5ae9_83750598', 'content');
?>


<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'content'} */
class Block_2557905545d7e233acc5ae9_83750598 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_2557905545d7e233acc5ae9_83750598',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-info">
        <h4>INFO!</h4>
        <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
      </div>
    </section>
  <?php }?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-md-4">
            <form method="POST" action="<?php echo base_url();?>
_admin/settings_controller/update_cost_personal">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Biaya member personal</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                <input type="text" name="member" value="<?php echo get_metsos('personal');?>
" class="form-control" required>
              </div><!-- ./box-body -->
              <div class="box-footer">
                <input type="submit" class="btn btn-info pull-right" name="submit" value="Ubah">     
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->          
          </form>
          <form method="POST" action="<?php echo base_url();?>
_admin/settings_controller/update_cost_personal_trainer">
          <div class="col-md-4">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Biaya member personal + trainer</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                <input type="text" name="member" value="<?php echo get_metsos('personal trainer');?>
" class="form-control" required>
              </div><!-- ./box-body -->
              <div class="box-footer">
                <input type="submit" class="btn btn-info pull-right" name="submit" value="Ubah">     
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->          
          </form>
          <form method="POST" action="<?php echo base_url();?>
_admin/settings_controller/update_cost_member_day">
          <div class="col-md-4">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Biaya member harian</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                <input type="text" name="member" value="<?php echo get_metsos('member_day');?>
" class="form-control" required>
              </div><!-- ./box-body -->
              <div class="box-footer">
                <input type="submit" class="btn btn-info pull-right" name="submit" value="Ubah">     
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->          
        </diV>
    </section><!-- /.content -->
    </form>
<?php
}
}
/* {/block 'content'} */
}
