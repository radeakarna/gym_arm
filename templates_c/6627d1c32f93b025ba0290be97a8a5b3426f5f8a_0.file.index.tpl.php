<?php
/* Smarty version 3.1.33, created on 2019-09-22 17:48:37
  from 'C:\laragon\www\gym\application\modules\_admin\views\member\personal\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d87b3f581a4e2_68212110',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6627d1c32f93b025ba0290be97a8a5b3426f5f8a' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\member\\personal\\index.tpl',
      1 => 1569173906,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d87b3f581a4e2_68212110 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18134866035d87b3f58044e7_65668983', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4817576015d87b3f5809d17_96604739', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6516170715d87b3f5815831_80108266', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_18134866035d87b3f58044e7_65668983 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_18134866035d87b3f58044e7_65668983',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- data tables-->
    <link href="<?php echo base_url();?>
plugins/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_4817576015d87b3f5809d17_96604739 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_4817576015d87b3f5809d17_96604739',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
              <a href="<?php echo base_url();?>
_admin/payment_personal/"><button type="button" class="btn btn-default btn-flat btn-sm"><i class="fa fa-money"></i> Pembayaran member</button></a>
                <a href="<?php echo base_url();?>
_admin/personal/create"><button type="button" class="btn btn-info btn-flat btn-sm"><i class="fa fa-plus"></i> Tambah data</button></a>
            </div><!-- /. tools -->
        </div>
        <div class="box-body">
          <form method="POST">
            <input type="hidden" id='csrf_test_name' name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
          </form>
          <div class="table-responsive">
            <table id="data-member" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>Nama</th>
                        <th>Kode member</th>
                        <th>Jenis member</th>
                        <th>Tangal daftar</th>
                        <th>Habis member</th>
                        <th><center>action</center></th>
                    </tr>
                </thead>
                <tfoot>

                </tfoot>
            </table>  
          </div>
        </div>
        <div class="box-footer clearfix">
          
        </div>
    </div>

</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_6516170715d87b3f5815831_80108266 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_6516170715d87b3f5815831_80108266',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

 <!-- data tables -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/media/js/jquery.dataTables.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/media/js/dataTables.bootstrap.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/admin/js/member.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
var menu_oTables="";
$(document).ready(function(){
  menu_oTables = $('#data-member').DataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": true,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": true,

       "ajax": {
          "url": '<?php echo base_url();?>
_admin/personal/show',
          "type": "POST",
          "dataType" : "json",
          "data": function ( d ) {
              d.csrf_test_name = $('#csrf_test_name').val();
          },
       },
       "columns": [
          { 
            "data": "no" 
          },
          { 
            "data": "name" 
          },
          { 
            "data": "id" 
          },
          { 
            "data": "type" 
          },
          { 
            "data": "created_at" 
          },
          { 
            "data": "lost_member" 
          },
          {
           "data": "action" 
         },
          ]
  });
});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'footer'} */
}
