<?php
/* Smarty version 3.1.33, created on 2019-09-15 09:19:53
  from 'C:\laragon\www\gym\application\views\contact.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7e02393100f9_37638960',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fabaa749b9a7c1473d2884ec6cc738674059fe2d' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\views\\contact.tpl',
      1 => 1568539190,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7e02393100f9_37638960 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php echo $_smarty_tpl->tpl_vars['tittle']->value;?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_471046465d7e0239309e84_98999972', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12029600015d7e023930f615_81955223', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "layouts/index.tpl");
}
/* {block 'content'} */
class Block_471046465d7e0239309e84_98999972 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_471046465d7e0239309e84_98999972',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<style type="text/css">
.breadcrumb_block{
    background-color:#28a745;
}
</style>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper">
    <div class="container">
        <div class="row justify-content-center" style="padding-top:100px;">
            <div class="col-md-4">
                <div class="breadcrumb_inner">
                    <h3>contact us</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb_block">
        <ul>
            <li><a href="index.html">home</a></li>
            <li>contact us</li>
        </ul>
    </div>
</div>
<!--Contact Block-->
<div class="contact_blocks_wrapper clv_section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="contact_block">
                    <span></span>
                    <div class="contact_icon"><img src="<?php echo base_url();?>
assets/user/images/contact_icon1.png" alt="image" /></div>
                    <h4>contact us</h4>
                    <p><?php echo get_metsos('contact');?>
</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="contact_block">
                    <span></span>
                    <div class="contact_icon"><img src="<?php echo base_url();?>
assets/user/images/contact_icon2.png" alt="image" /></div>
                    <h4>email</h4>
                    <p><?php echo get_metsos('gmail');?>
</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="contact_block">
                    <span></span>
                    <div class="contact_icon"><img src="<?php echo base_url();?>
assets/user/images/contact_icon3.png" alt="image" /></div>
                    <h4>address</h4>
                    <p><?php echo get_metsos('addres_gym');?>
</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_12029600015d7e023930f615_81955223 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_12029600015d7e023930f615_81955223',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        
<?php
}
}
/* {/block 'footer'} */
}
