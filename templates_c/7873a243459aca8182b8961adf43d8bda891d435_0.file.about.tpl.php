<?php
/* Smarty version 3.1.33, created on 2019-09-15 11:42:22
  from 'C:\laragon\www\gym\application\modules\_admin\views\settings\about.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7e239e7a7c36_44921170',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7873a243459aca8182b8961adf43d8bda891d435' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\settings\\about.tpl',
      1 => 1568547028,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7e239e7a7c36_44921170 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11483742925d7e239e796899_45562147', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20113935745d7e239e7a56d8_42668489', 'footer');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'content'} */
class Block_11483742925d7e239e796899_45562147 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_11483742925d7e239e796899_45562147',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="callout callout-success">
        <h4>INFO!</h4>
        <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
      </div>
    </section>
  <?php }?>
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">About US</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div><!-- /.box-header -->
              <div class="box-body">
                <form method="POST" action="<?php echo base_url();?>
_admin/setting/update_about">
                    <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
                    <div class="form-group">
                      <textarea class="form-control" name="about" id="about"><?php echo get_metsos('about');?>
</textarea>
                    </div>
                    <div class="form-group">
                      <input type="submit" class="btn btn-info" name="submit" value="submit">
                    </div>
                </form>
                <div class="row">


                
              </div><!-- ./box-body -->
              <div class="box-footer">
                
              </div><!-- /.box-footer -->
            </div><!-- /.box -->
          </div><!-- /.col -->

          
        </diV>
    </section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_20113935745d7e239e7a56d8_42668489 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_20113935745d7e239e7a56d8_42668489',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Tiny Mce / untuk membuat tampilan type text menjadi tampilan blogspot -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/tinymce/tinymce.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    tinymce.init({
        selector: "#about",theme: "modern",height: 400,
        plugins: [
             "advlist autolink link image lists charmap print preview hr anchor pagebreak",
             "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
             "table contextmenu directionality emoticons paste textcolor"
       ],
       toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
       toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
       image_advtab: true ,
       external_filemanager_path:"<?php echo base_url();?>
plugins/filemanager/",
       filemanager_title:"Responsive Filemanager" ,
       external_plugins: { "filemanager" : "../filemanager/plugin.min.js"}
    });
<?php echo '</script'; ?>
>    
<?php
}
}
/* {/block 'footer'} */
}
