<?php
/* Smarty version 3.1.33, created on 2019-10-29 13:45:15
  from 'C:\laragon\www\gym\application\modules\_admin\views\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db8426b92d718_96750924',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2cea52b2a6342fee31908d9a4be71ccc759fa07c' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\index.tpl',
      1 => 1572355480,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db8426b92d718_96750924 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo (($tmp = @$_smarty_tpl->tpl_vars['title']->value)===null||$tmp==='' ? 'Admin | SD MUHAMMADIYAH 16 SURAKARTA' : $tmp);?>
</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>
assets/admin/css/bootstrap.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url();?>
assets/admin/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>
assets/admin/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>
assets/admin/css/skins/_all-skins.css">
    <!-- data tables-->
    <link href="<?php echo base_url();?>
plugins/media/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Sweet Alert -->
    <link href="<?php echo base_url();?>
plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5429978415db8426b918152_13268386', 'header');
?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->
  </head>
  <body class="hold-transition skin-green-light sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>AD</b>MIN web</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>ADMIN | W</b>eb</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url();?>
assets/admin/img/log.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url();?>
assets/admin/img/log.png" class="img-circle" alt="User Image">
                    <p>
                      
                      <small></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url();?>
_admin/set/index" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url();?>
_admin/auth_controller/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            
          </div>
          
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="<?php echo base_url();?>
_admin/">
                <i class="fa fa-home"></i> <span>HOME</span> 
              </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Member</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <!-- <li><a href="<?php echo base_url();?>
_admin/member/"><i class="fa fa-angle-double-right"></i> Daftar member</a></li>
                    <li><a href="<?php echo base_url();?>
_admin/member/create"><i class="fa fa-angle-double-right"></i> Tambah member</a></li>
                    <li><a href="<?php echo base_url();?>
_admin/setting/cost"><i class="fa fa-angle-double-right"></i> Setting Biaya</a></li> -->
                    <!-- <li><a href="<?php echo base_url();?>
_admin/setting/cost"><i class="fa fa-angle-double-right"></i>Perpanjang member</a></li> -->
                    <li>
                      <a href="#"><i class="fa fa-angle-double-right"></i> Individu <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <!-- <li><a href="<?php echo base_url();?>
_admin/individu/"><i class="fa fa-circle-o"></i> Daftar member</a></li>
                        <li><a href="<?php echo base_url();?>
_admin/individu/create"><i class="fa fa-circle-o"></i> Tambah member</a></li> -->
                        <li><a href="<?php echo base_url();?>
_admin/payment_individu"><i class="fa fa-circle-o"></i> Data Pembayaran</a></li>
                        <li><a href="<?php echo base_url();?>
_admin/payment_individu/create"><i class="fa fa-circle-o"></i> Pembayaran</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-angle-double-right"></i> Personal <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="<?php echo base_url();?>
_admin/personal/"><i class="fa fa-circle-o"></i> Daftar member</a></li>
                        <li><a href="<?php echo base_url();?>
_admin/personal/create"><i class="fa fa-circle-o"></i> Tambah member</a></li>
                        <li><a href="<?php echo base_url();?>
_admin/personal_presences"><i class="fa fa-circle-o"></i> Presensi</a></li>
                      </ul>
                    </li>
                    <li>
                      <a href="#"><i class="fa fa-angle-double-right"></i> Personal trainer <i class="fa fa-angle-left pull-right"></i></a>
                      <ul class="treeview-menu">
                        <li><a href="<?php echo base_url();?>
_admin/personal_trainer/"><i class="fa fa-circle-o"></i> Daftar member</a></li>
                        <li><a href="<?php echo base_url();?>
_admin/personal_trainer/create"><i class="fa fa-circle-o"></i> Tambah member</a></li>
                        <li><a href="<?php echo base_url();?>
_admin/personal_trainer_presences"><i class="fa fa-circle-o"></i> Presensi</a></li>
                      </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Chart</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                      <a href="<?php echo base_url();?>
_admin/chart_controller"><i class="fa fa-angle-double-right"></i> Individu <i class="fa fa-angle-left pull-right"></i></a>
                      <!-- <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Harian</a></li>
                      </ul> -->
                    </li>
                    <li>
                      <a href="<?php echo base_url();?>
_admin/chart_controller/personal"><i class="fa fa-angle-double-right"></i> Personal <i class="fa fa-angle-left pull-right"></i></a>
                      
                    </li>
                    <li>
                      <a href="<?php echo base_url();?>
_admin/chart_controller/personal_trainer"><i class="fa fa-angle-double-right"></i> Personal trainer <i class="fa fa-angle-left pull-right"></i></a>
                      
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i> <span>Pages</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url();?>
_admin/setting/about"><i class="fa fa-angle-double-right"></i> About us</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-gear"></i> <span>Setting</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url();?>
_admin/trainer/"><i class="fa fa-angle-double-right"></i> Trainer</a></li>
                    <li><a href="<?php echo base_url();?>
_admin/setting/"><i class="fa fa-angle-double-right"></i> metsos</a></li>
                    <li><a href="<?php echo base_url();?>
_admin/setting/cost"><i class="fa fa-angle-double-right"></i>Biaya</a></li>
                </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_672595165db8426b926d23_13587740', 'content');
?>

      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          
        </div>
       <strong>Copyright &copy; 2019. </strong>
      </footer>

      
    <!-- jQuery 2.1.4 -->
    <?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/admin/js/jQuery-2.1.4.min.js"><?php echo '</script'; ?>
>
    <!-- Bootstrap 3.3.5 -->
    <?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/admin/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <!-- AdminLTE App -->
    <?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/admin/js/app.min.js"><?php echo '</script'; ?>
>
    <!-- Sweet Alert -->
    <?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/sweetalert/sweetalert.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/sweetalert/swalert.js" type="text/javascript"><?php echo '</script'; ?>
>
    <!-- Sidebar menu -->
    <?php echo '<script'; ?>
 type="text/javascript">
        /** add active class and stay opened when selected */
        var url = window.location;
        // for sidebar menu entirely but not cover treeview
        $('ul.sidebar-menu a').filter(function() {
             return this.href == url;
        }).parent().addClass('active');
        // for treeview
        $('ul.treeview-menu a').filter(function() {
             return this.href == url;
        }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
    <?php echo '</script'; ?>
>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12159661895db8426b92b7f6_31946845', 'footer');
?>

  </body>
</html>
<?php }
/* {block 'header'} */
class Block_5429978415db8426b918152_13268386 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_5429978415db8426b918152_13268386',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    
    <?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_672595165db8426b926d23_13587740 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_672595165db8426b926d23_13587740',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <section class="content">
          <!-- quick email widget -->
          <div class="box box-info">
              <div class="box-header">
                  <i class="fa fa-home"></i>
                  <h3 class="box-title">Dasboard</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                      
                  </div><!-- /. tools -->
              </div>
              <div class="box-body">
                  <div class="callout callout-info">
                    <h1>SELAMAT DATANG DI ADMIN WEB</h1>
                    <h3>Silahkan pilih menu bagian kiri untuk memulai</h3>
                  </div>
              </div>
              <div class="box-footer clearfix">
              </div>
          </div>

      </section><!-- /.content -->
        <!-- Main content -->
        <section class="content">
          
        </section><!-- /.content -->
        <?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_12159661895db8426b92b7f6_31946845 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_12159661895db8426b92b7f6_31946845',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    
    <?php
}
}
/* {/block 'footer'} */
}
