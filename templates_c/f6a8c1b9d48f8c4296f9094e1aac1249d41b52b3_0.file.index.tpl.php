<?php
/* Smarty version 3.1.33, created on 2019-10-06 07:37:18
  from 'C:\laragon\www\gym\application\modules\_admin\views\chart\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d9999ae56ced9_06021019',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f6a8c1b9d48f8c4296f9094e1aac1249d41b52b3' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\chart\\index.tpl',
      1 => 1570347377,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d9999ae56ced9_06021019 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12948664175d9999ae55fe35_11858349', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17027911745d9999ae565773_81564660', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2479750565d9999ae5667a5_00186300', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_12948664175d9999ae55fe35_11858349 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_12948664175d9999ae55fe35_11858349',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Morris Charts CSS -->
<link href="<?php echo base_url();?>
plugins/moris_chart/morris.css" rel="stylesheet">
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_17027911745d9999ae565773_81564660 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_17027911745d9999ae565773_81564660',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<section class="content">
    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan harian</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan bulanan</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart-bulan" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

    <!-- Bar chart -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-bar-chart-o"></i>
        <h3 class="box-title">Penghasilan Pertahun</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div id="morris-bar-chart-tahun" style="height: 300px;"></div>
      </div><!-- /.box-body-->
    </div><!-- /.box -->

</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_2479750565d9999ae5667a5_00186300 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_2479750565d9999ae5667a5_00186300',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<!-- Morris Charts JavaScript -->
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/moris_chart/raphael.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo base_url();?>
plugins/moris_chart/morris.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"<?php echo base_url();?>
_admin/chart_controller/get_data_day",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});

$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart-bulan',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"<?php echo base_url();?>
_admin/chart_controller/get_data_month",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});

$(function() {
    var cart = Morris.Bar({
        element: 'morris-bar-chart-tahun',
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Penghasilan'],
        hideHover: 'auto',
        resize: true
    });

    $.ajax({
        type:"GET",
        dataType:"json",
        url:"<?php echo base_url();?>
_admin/chart_controller/get_data_year",
        success:function(data) {
          console.log(data);
           cart.setData(data);
        }
    });

    
});
<?php echo '</script'; ?>
>

<?php
}
}
/* {/block 'footer'} */
}
