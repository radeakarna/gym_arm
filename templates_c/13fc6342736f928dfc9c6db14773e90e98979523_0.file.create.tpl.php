<?php
/* Smarty version 3.1.33, created on 2019-10-29 14:34:04
  from 'C:\laragon\www\gym\application\modules\_admin\views\trainer\create.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db84ddc7be035_93428966',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '13fc6342736f928dfc9c6db14773e90e98979523' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\trainer\\create.tpl',
      1 => 1572359642,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db84ddc7be035_93428966 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2414515635db84ddc7ab2f5_92090471', 'header');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20807622825db84ddc7b0a96_33129953', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8480645345db84ddc7bd5a3_83684349', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'header'} */
class Block_2414515635db84ddc7ab2f5_92090471 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header' => 
  array (
    0 => 'Block_2414515635db84ddc7ab2f5_92090471',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<link rel="stylesheet" href="<?php echo base_url();?>
plugins/datetimepicker/css/bootstrap-datepicker.css">
<?php
}
}
/* {/block 'header'} */
/* {block 'content'} */
class Block_20807622825db84ddc7b0a96_33129953 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_20807622825db84ddc7b0a96_33129953',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Trainer</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="<?php echo base_url();?>
_admin/trainer/create" enctype='multipart/form-data'>
        <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" class="form-control" name="name" placeholder="Nama lengkap" value="" required/>
              <?php echo form_error('name');?>

            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Jenis kelamin</label>
              <select name="jk" class="form-control">
                <option value="laki-laki">laki-laki</option>
                <option value="perempuan">perempuan</option>
              </select>
              <?php echo form_error('jk');?>

            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Alamat</label>
              <textarea name="alamat" class="form-control" rows="4"></textarea>
              <?php echo form_error('alamat');?>

            </div>

            <div class="form-group">
              <label for="exampleInputEmail1">Foto</label>
              <input type="file" name="foto" required/>
              <?php echo form_error('foto');?>

            </div>
          
        </div>
        <div class="box-footer clearfix">
          <button type="submit" class="btn btn-primary">Simpan data</button>
        </div>
        </form>
    </div>
</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_8480645345db84ddc7bd5a3_83684349 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_8480645345db84ddc7bd5a3_83684349',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php
}
}
/* {/block 'footer'} */
}
