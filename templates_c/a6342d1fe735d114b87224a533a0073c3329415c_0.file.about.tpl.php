<?php
/* Smarty version 3.1.33, created on 2019-09-24 15:49:39
  from 'C:\laragon\www\gym\application\views\about.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d8a3b13c42237_89932899',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a6342d1fe735d114b87224a533a0073c3329415c' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\views\\about.tpl',
      1 => 1569340175,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d8a3b13c42237_89932899 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php echo $_smarty_tpl->tpl_vars['tittle']->value;?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4373913645d8a3b13c3f438_77186691', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "layouts/index.tpl");
}
/* {block 'content'} */
class Block_4373913645d8a3b13c3f438_77186691 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_4373913645d8a3b13c3f438_77186691',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<style type="text/css">
.breadcrumb_block{
    background-color:#28a745;
}
.breadcrumb_wrapper{
    background: url('../images/hrader-1.jpg');
}
</style>
<!--Breadcrumb-->
<div class="breadcrumb_wrapper" style="background:url('assets/user/images/hrader-1.jpg') !important">
    <div class="container">
        <div class="row justify-content-center" style="padding-top:100px;">
            <div class="col-md-4">
                <div class="breadcrumb_inner">
                    <h3>About us</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="breadcrumb_block">
        <ul>
            <li><a href="index.html">home</a></li>
            <li>about us</li>
        </ul>
    </div>
</div>
<!--Blog With Sidebar-->
<div class="blog_sidebar_wrapper clv_section blog_single_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="blog_left_section">
                    <div class="blog_section">
                        <div class="agri_blog_image">
                            <!-- <img src="https://via.placeholder.com/870x410" alt="image"> -->
                            <!-- <span class="agri_blog_date">About us</span> -->
                        </div>
                        <div class="agri_blog_content">
                            <?php echo get_metsos('about');?>

                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<?php
}
}
/* {/block 'content'} */
}
