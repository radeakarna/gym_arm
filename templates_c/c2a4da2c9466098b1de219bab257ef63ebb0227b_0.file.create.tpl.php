<?php
/* Smarty version 3.1.33, created on 2019-09-22 16:59:31
  from 'C:\laragon\www\gym\application\modules\_admin\views\pembayaran\personal_trainer\create.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d87a873bbd802_85989307',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c2a4da2c9466098b1de219bab257ef63ebb0227b' => 
    array (
      0 => 'C:\\laragon\\www\\gym\\application\\modules\\_admin\\views\\pembayaran\\personal_trainer\\create.tpl',
      1 => 1569171430,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d87a873bbd802_85989307 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5109145725d87a873bad1b9_16301965', 'content');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7355097495d87a873bbb6d7_14341211', 'footer');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "index.tpl");
}
/* {block 'content'} */
class Block_5109145725d87a873bad1b9_16301965 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_5109145725d87a873bad1b9_16301965',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="callout callout-info">
      <h4>INFO!</h4>
      <p><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p>
    </div>
  </section>
<?php }?>
<section class="content">
    <!-- quick email widget -->
    <div class="box box-success">
        <div class="box-header">
            <i class="fa fa-user"></i>
            <h3 class="box-title">Member</h3>
            <!-- tools box -->
            <div class="pull-right box-tools">
                
            </div><!-- /. tools -->
        </div>
        <form method="POST" action="<?php echo base_url();?>
_admin/payment_personal_trainer/create">
        <div class="box-body">
            <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" style="display: none">
            <div class="input-group">
              <input type="text" class="form-control" name="id_member" id="id_member" placeholder="Kode member" value="" required/>
              <span class="input-group-btn">
                <button type="submit" class="btn btn-success btn-flat">Bayar</button>
              </span>
            </div>
        </div>
        <div class="box-footer clearfix">
          <!-- <button type="submit" class="btn btn-primary">Simpan data</button> -->
        </div>
        </form>
    </div>
</section><!-- /.content -->
<?php
}
}
/* {/block 'content'} */
/* {block 'footer'} */
class Block_7355097495d87a873bbb6d7_14341211 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_7355097495d87a873bbb6d7_14341211',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php echo '<script'; ?>
 type="text/javascript">
//     $(document).ready(function(){
//         $("#id_member").change(function(){
//             var id_member = $(this).val();
//             $.ajax({
//             type: "POST",
//             data: {
//                 csrf_test_name:<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
, id:id_member
//             },
//             dataType:"JSON",
//             url: "<?php echo base_url();?>
_admin/payment_individu/find_user",
//               success: function(data) {
//                   $('[name="name"]').val(data.name);
//               },
//               error: function (jqXHR, textStatus, errorThrown)
//               {
//                 swal("Error showing!", "Please try again", "error");
//               }
//             });
//         });
//     })
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'footer'} */
}
